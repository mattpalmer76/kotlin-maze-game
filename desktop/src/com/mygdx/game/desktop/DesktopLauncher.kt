package com.mygdx.game.desktop

import kotlin.jvm.JvmStatic
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.mygdx.game.config.Constants
import com.mygdx.game.DragonGame
import net.spookygames.gdx.nativefilechooser.desktop.DesktopFileChooser

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()

        // basic confirguration for this game
        config.title = "Maze Monkey"
        config.useGL30 = false
        config.width = Constants.SCREEN_WIDTH
        config.height = Constants.SCREEN_HEIGHT
        config.resizable = false

        // we pass the game a desktop file chooser when beginning game
        LwjglApplication(DragonGame(DesktopFileChooser()), config)
    }
}