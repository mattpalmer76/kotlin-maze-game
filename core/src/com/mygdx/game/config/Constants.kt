package com.mygdx.game.config

// class containing all constants
class Constants {
    companion object {
        // screen size
        public const val SCREEN_WIDTH : Int = 800
        public const val SCREEN_HEIGHT : Int = 608

        // scaling of graphics (eg x2)
        public const val SCALING : Int = 2

        // size of tiles
        public const val TILE_WIDTH : Int = 16
        public const val TILE_HEIGHT : Int = 16

        // size of each character in font (assuming all characters same size)
        public const val CHAR_WIDTH : Int = 16
        public const val CHAR_HEIGHT : Int = 16

        // scaled tile sizes
        public const val SCALED_TILE_WIDTH : Int = TILE_WIDTH * SCALING
        public const val SCALED_TILE_HEIGHT : Int = TILE_HEIGHT * SCALING

        // size of levels
        public const val TILES_ACROSS : Int = 1 + SCREEN_WIDTH / SCALED_TILE_WIDTH
        public const val TILES_UP : Int = 1 + SCREEN_HEIGHT / SCALED_TILE_HEIGHT

        // number of levels assigned to a game
        public const val NUM_OF_LEVELS : Int = 20

        // max amount of blocks available
        public const val BLOCK_COUNT : Int = 49
        public const val CLIMBABLE_COUNT : Int = 12
        public const val ITEM_COUNT : Int = 13
        public const val OBSTACLE_COUNT : Int = 7
        public const val ANIM_COUNT : Int = 4

        // number of enemies
        public const val ENEMIES_COUNT : Int = 1

        // prefix of regions in certain texture atlantes
        public const val BLOCK_NAME : String = "block"
        public const val CLIMBABLE_NAME : String = "climbable"
        public const val ITEM_NAME : String = "item"
        public const val OBSTACLE_NAME : String = "obstacle"

        // main gui skin path
        public const val SKIN_PATH : String = "skins/c64/uiskin.json"

        // path to regular blocks
        public const val BLOCKS_PATH : String = "blocks/blocks.atlas"
        public const val CLIMBABLES_PATH : String = "climbables/climbables.atlas"
        public const val ITEMS_PATH : String = "items/items.atlas"
        public const val OBSTACLES_PATH : String = "obstacles/obstacles.atlas"

        // path to each anim block
        public const val CANDLE_ANIM_PATH : String = "animated/candle/candle.atlas"
        public const val FIRE1_ANIM_PATH : String = "animated/fire1/fire1.atlas"
        public const val SPIDER_ANIM_PATH : String = "animated/spider/spider.atlas"
        public const val WALLTORCH_ANIM_PATH : String = "animated/walltorch/walltorch.atlas"

        // index for enemies
        public const val ENEMY_0_INDEX : Int = 0;

        // path to player texture atlantes
        public const val PLAYER_IDLE_PATH : String = "sprites/player/idle/idle.atlas"
        public const val PLAYER_UP_PATH : String = "sprites/player/up/up.atlas"
        public const val PLAYER_DOWN_PATH : String = "sprites/player/down/down.atlas"
        public const val PLAYER_LEFT_PATH : String = "sprites/player/left/left.atlas"
        public const val PLAYER_RIGHT_PATH : String = "sprites/player/right/right.atlas"

        // path to each enemy 0 movements
        public const val ENEMY_0_IDLE_PATH : String = "sprites/enemies/0/idle/idle.atlas"
        public const val ENEMY_0_UP_PATH : String = "sprites/enemies/0/up/up.atlas"
        public const val ENEMY_0_DOWN_PATH : String = "sprites/enemies/0/down/down.atlas"
        public const val ENEMY_0_LEFT_PATH: String = "sprites/enemies/0/left/left.atlas"
        public const val ENEMY_0_RIGHT_PATH: String = "sprites/enemies/0/right/right.atlas"

        // directory names
        public const val SAVE_DIRECTORY : String = "save"
        public const val CONFIG_DIRECTORY : String = "config"
        public const val LEVEL_DIRECTORY : String = "level"

        // file names
        public const val LEVEL_SELECTION_FILENAME : String = "level_selection"

        // file extensions
        public const val MAP_EXTENSION = ".map"

        // player movement area (outside here causes screen to scroll
        public const val PLAYER_MOVEMENT_AREA_WIDTH = 400
        public const val PLAYER_MOVEMENT_AREA_HEIGHT = 300
        public const val PMA_X1 = (SCREEN_WIDTH - PLAYER_MOVEMENT_AREA_WIDTH) / 2
        public const val PMA_X2 = PMA_X1 + PLAYER_MOVEMENT_AREA_WIDTH
        public const val PMA_Y1 = (SCREEN_HEIGHT - PLAYER_MOVEMENT_AREA_HEIGHT) / 2
        public const val PMA_Y2 = PMA_Y1 + PLAYER_MOVEMENT_AREA_HEIGHT

    }
}