package com.mygdx.game.helper

import com.badlogic.gdx.files.FileHandle
import com.mygdx.game.config.Constants
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

class GameFiles {
    companion object {
        // location where this game is being executed
        var baseDirectory = File("").absolutePath

        // various game locations
        var saveDir = File(baseDirectory + "\\" + Constants.SAVE_DIRECTORY)
        var configDir = File(baseDirectory + "\\" + Constants.CONFIG_DIRECTORY)
        var levelDir = File(baseDirectory + "\\" + Constants.LEVEL_DIRECTORY)

        // contains a list of levels which will be used while playing the game
        // is an 'encoded' path
        var levelSelection = mutableMapOf<Int, String>();

        init {
            // initialise level selection to empty
            for(i in 0 until Constants.NUM_OF_LEVELS) {
                levelSelection[i] = ""
            }
        }

        // create all dirs if not existing
        fun checkIfDirs() {
            checkIfSaveDir()
            checkIfConfigDir()
            checkIfLevelDir()
        }

        fun checkIfSaveDir() {
            if(!saveDir.exists())
                saveDir.mkdir()
        }

        fun checkIfConfigDir() {
            if(!configDir.exists())
                configDir.mkdir()
        }

        fun checkIfLevelDir() {
            if(!levelDir.exists())
                levelDir.mkdir()
        }

        // loads the list of available levels from relevant directory
        fun loadLevelSelection() : Map<Int, String> {
            var levelSelectionJA = JSONArray()

            try {
                var levelSelectionFile = FileHandle(configDir.absolutePath + "\\" + Constants.LEVEL_SELECTION_FILENAME);
                if(levelSelectionFile.exists())
                    levelSelectionJA = JSONArray(levelSelectionFile.readString())

                for(i in 0 until levelSelectionJA.length()) {
                    var levelWithNumber = levelSelectionJA.getJSONObject(i)
                    levelSelection[levelWithNumber.getInt("number")] = levelWithNumber.getString("filename")
                }
            } catch(e : Exception) {
                println("There was an error parsing the level selection file while loading.")
                e.printStackTrace()
            }

            removeBlankSpacesFromLevels()

            return levelSelection;
        }

        // saves the level selections
        fun saveLevelSelection(levelSelection : Map<Int, String> = this.levelSelection) : Boolean {
            var levelSelectionJA = JSONArray()

            removeBlankSpacesFromLevels()

            try {
                for((number, filename) in levelSelection) {
                    var levelWithNumber = JSONObject()
                    levelWithNumber.put("number", number)
                    levelWithNumber.put("filename", filename)

                    levelSelectionJA.put(levelWithNumber)
                }

                var levelSelectionFile = FileHandle(configDir.absolutePath + "\\" + Constants.LEVEL_SELECTION_FILENAME);
                levelSelectionFile.writeString(levelSelectionJA.toString(), false)

                return true
            } catch (e : Exception) {
                println("There was an error saving the level selection file.")
                e.printStackTrace()
                return false
            }
        }

        // removes blank entries between level selections
        fun removeBlankSpacesFromLevels() {

            var keepLooping = true

            while(keepLooping) {
                var foundBlank = false
                for(i in 0 until Constants.NUM_OF_LEVELS) {
                    if(levelSelection[i].isNullOrEmpty()) {
                        foundBlank = true
                    } else {
                        if(foundBlank) {
                            for(j in i until Constants.NUM_OF_LEVELS) {
                                levelSelection[j-1] = levelSelection[j]!!
                            }
                            levelSelection[Constants.NUM_OF_LEVELS - 1] = ""
                            break;
                        }
                    }
                    if(i >= Constants.NUM_OF_LEVELS - 1) {
                        keepLooping = false;
                    }
                }
            }
        }

        // todo load game save

        // todo save game save
    }
}