package com.mygdx.game.helper

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

class GlobalKeyStates {
    // some keys are best read from a single source, such as ESC
    // this is to avoid multiple reactions to a keypress causing problems
    companion object {
        public val esc = KeyState(false, Input.Keys.ESCAPE)
        public val space = KeyState(false, Input.Keys.SPACE)
        public val enter = KeyState(false, Input.Keys.ENTER)
    }
}
