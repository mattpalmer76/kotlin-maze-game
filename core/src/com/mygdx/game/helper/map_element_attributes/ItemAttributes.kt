package com.mygdx.game.helper.map_element_attributes

class ItemAttributes {
    companion object {
        public enum class ItemAttribute {
            EMPTY,
            SWORD,
            ROPE,
            EMPTY_BUCKET,
            FULL_BUCKET,
            PLANK,
            BUG_SPRAY,
            LIFE_RING,
            BOOTS,
            MONEY_BAG,
            COIN,
            KEY,
            TORCH
        }

        var itemNames = mapOf<ItemAttribute, String>(
                ItemAttribute.EMPTY to "Empty",
                ItemAttribute.SWORD to "Sword",
                ItemAttribute.ROPE to "Rope",
                ItemAttribute.EMPTY_BUCKET to "Empty Bucket",
                ItemAttribute.FULL_BUCKET to "Full Bucket",
                ItemAttribute.PLANK to "Plank",
                ItemAttribute.BUG_SPRAY to "Bug Spray",
                ItemAttribute.LIFE_RING to "Life Ring",
                ItemAttribute.BOOTS to "Boots",
                ItemAttribute.MONEY_BAG to "Money Bag",
                ItemAttribute.COIN to "Coin",
                ItemAttribute.KEY to "Key",
                ItemAttribute.TORCH to "Torch"
        )

        var itemAttributes = mapOf<Int, ItemAttribute>(
                0 to ItemAttribute.EMPTY,
                1 to ItemAttribute.SWORD,
                2 to ItemAttribute.ROPE,
                3 to ItemAttribute.EMPTY_BUCKET,
                4 to ItemAttribute.FULL_BUCKET,
                5 to ItemAttribute.PLANK,
                6 to ItemAttribute.BUG_SPRAY,
                7 to ItemAttribute.LIFE_RING,
                8 to ItemAttribute.BOOTS,
                9 to ItemAttribute.MONEY_BAG,
                10 to ItemAttribute.COIN,
                11 to ItemAttribute.KEY,
                12 to ItemAttribute.TORCH
        )

        fun getItemEnum(item : Int) : ItemAttribute? {
            return itemAttributes[item]
        }

        fun getItemNumber(itemAttribute : ItemAttribute) : Int {
            for ((key, value) in itemAttributes) {
                if(value == itemAttribute)
                    return key
            }

            return 0
        }
    }
}