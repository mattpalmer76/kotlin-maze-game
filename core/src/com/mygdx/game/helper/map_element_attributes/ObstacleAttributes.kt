package com.mygdx.game.helper.map_element_attributes

import com.mygdx.game.model.GameLevel

class ObstacleAttributes {
    companion object {
        public enum class ObstacleAttribute {
            EMPTY,
            WATER,
            MUD
        }

        var itemAttributes = mapOf<Int, ObstacleAttribute>(
                0 to ObstacleAttribute.EMPTY,
                1 to ObstacleAttribute.WATER,
                2 to ObstacleAttribute.WATER,
                3 to ObstacleAttribute.WATER,
                4 to ObstacleAttribute.MUD,
                5 to ObstacleAttribute.MUD,
                6 to ObstacleAttribute.MUD
        )

        fun isObstacle(x : Int, y : Int, level : GameLevel) : Boolean {
            if(getAttributeForNum(level.getObstacleXY(x, y)) == ObstacleAttribute.WATER ||
                    getAttributeForNum(level.getObstacleXY(x, y)) == ObstacleAttribute.MUD) {
                return true
            }
            return false
        }

        fun isWater(x : Int, y : Int, level : GameLevel) : Boolean {
            if(getAttributeForNum(level.getObstacleXY(x, y)) == ObstacleAttribute.WATER) {
                return true
            }
            return false
        }

        fun isMud(x : Int, y : Int, level : GameLevel) : Boolean {
            if(getAttributeForNum(level.getObstacleXY(x, y)) == ObstacleAttribute.MUD) {
                return true
            }
            return false
        }

        private fun getAttributeForNum(num : Int) : ObstacleAttribute {
            for((key, value) in itemAttributes) {
                if(key == num)
                    return value
            }
            return ObstacleAttribute.EMPTY
        }
    }
}