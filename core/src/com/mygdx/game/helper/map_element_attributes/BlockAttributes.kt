package com.mygdx.game.helper.map_element_attributes

import com.mygdx.game.model.GameLevel
import jdk.nashorn.internal.ir.Block

class BlockAttributes {
    companion object {
        public enum class BlockAttribute {
            EMPTY,
            BLOCKING,
            SEED,
            DOOR,
            END_LEVEL,
            LEFT_ONLY,
            RIGHT_ONLY,
            STAIR_UP_LEFT,
            STAIR_UP_RIGHT
        }

        val blockAttributes = mapOf<Int, BlockAttribute>(
                0 to BlockAttribute.EMPTY,
                1 to BlockAttribute.BLOCKING,
                2 to BlockAttribute.BLOCKING,
                3 to BlockAttribute.BLOCKING,
                4 to BlockAttribute.BLOCKING,
                5 to BlockAttribute.BLOCKING,
                6 to BlockAttribute.BLOCKING,
                7 to BlockAttribute.BLOCKING,
                8 to BlockAttribute.BLOCKING,
                9 to BlockAttribute.EMPTY,
                10 to BlockAttribute.LEFT_ONLY,
                11 to BlockAttribute.RIGHT_ONLY,
                12 to BlockAttribute.SEED,
                13 to BlockAttribute.DOOR,
                14 to BlockAttribute.END_LEVEL,
                15 to BlockAttribute.END_LEVEL,
                16 to BlockAttribute.END_LEVEL,
                17 to BlockAttribute.END_LEVEL,
                18 to BlockAttribute.END_LEVEL,
                19 to BlockAttribute.END_LEVEL,
                20 to BlockAttribute.END_LEVEL,
                21 to BlockAttribute.BLOCKING,
                22 to BlockAttribute.BLOCKING,
                23 to BlockAttribute.BLOCKING,
                24 to BlockAttribute.BLOCKING,
                25 to BlockAttribute.BLOCKING,
                26 to BlockAttribute.BLOCKING,
                27 to BlockAttribute.BLOCKING,
                28 to BlockAttribute.BLOCKING,
                29 to BlockAttribute.BLOCKING,
                30 to BlockAttribute.BLOCKING,
                31 to BlockAttribute.BLOCKING,
                32 to BlockAttribute.BLOCKING,
                33 to BlockAttribute.BLOCKING,
                34 to BlockAttribute.BLOCKING,
                35 to BlockAttribute.BLOCKING,
                36 to BlockAttribute.BLOCKING,
                37 to BlockAttribute.STAIR_UP_RIGHT,
                38 to BlockAttribute.STAIR_UP_LEFT,
                39 to BlockAttribute.EMPTY,
                40 to BlockAttribute.EMPTY,
                41 to BlockAttribute.STAIR_UP_RIGHT,
                42 to BlockAttribute.STAIR_UP_LEFT,
                43 to BlockAttribute.EMPTY,
                44 to BlockAttribute.EMPTY,
                45 to BlockAttribute.STAIR_UP_RIGHT,
                46 to BlockAttribute.STAIR_UP_LEFT,
                47 to BlockAttribute.EMPTY,
                48 to BlockAttribute.EMPTY
        )

        fun isBlocking(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.BLOCKING
            return false
        }

        fun isSeed(x: Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!!.equals(BlockAttribute.SEED)
            return false
        }

        fun isDoor(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.DOOR
            return false
        }

        fun isEndLevel(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.END_LEVEL
            return false
        }

        fun isLeftOnly(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.LEFT_ONLY
            return false
        }

        fun isRightOnly(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.RIGHT_ONLY
            return false
        }

        fun isStairUpLeft(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.STAIR_UP_LEFT
            return false
        }

        fun isStairUpRight(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) != null)
                return blockAttributes[level.getBlockXY(x, y)]!! == BlockAttribute.STAIR_UP_RIGHT
            return false
        }

        fun setPlank(x : Int, y : Int, level : GameLevel) : Boolean {
            if(level.getBlockXY(x, y) == 0) {
                level.setBlockXY(x, y, 8)
                return true
            }
            return false
        }

        fun clearBlock(x : Int, y : Int, level : GameLevel) {
            level.setBlockXY(x, y, 0)
        }
    }
}