package com.mygdx.game.helper.map_element_attributes

import com.mygdx.game.model.GameLevel
import kotlin.random.Random

class ClimbableAttributes {
    companion object {
        public enum class ClimbableAttribute {
            EMPTY,
            EMPTY_HOOP,
            CLIMBABLE_HOOP,
            CLIMBABLE,
            CLIMBABLE_ROPE,
            ROPE_END_CLIMBABLE,
            VINE_START,
            VINE_1,
            VINE_2,
            VINE_3
        }

        var randomNumForVine = Random.nextInt(0, 2)

        val climbableAttributes = mapOf<Int, ClimbableAttribute>(
                0 to ClimbableAttribute.EMPTY,
                1 to ClimbableAttribute.CLIMBABLE,
                2 to ClimbableAttribute.EMPTY_HOOP,
                3 to ClimbableAttribute.CLIMBABLE_HOOP,
                4 to ClimbableAttribute.CLIMBABLE_ROPE,
                5 to ClimbableAttribute.ROPE_END_CLIMBABLE,
                6 to ClimbableAttribute.VINE_START,
                7 to ClimbableAttribute.VINE_1,
                8 to ClimbableAttribute.VINE_2,
                9 to ClimbableAttribute.VINE_3,
                10 to ClimbableAttribute.CLIMBABLE,
                11 to ClimbableAttribute.CLIMBABLE
        )

        fun isClimbable(x: Int, y: Int, level : GameLevel) : Boolean {
            var attributeIs = level.getClimbableXY(x, y)
            if(attributeIs == 2) {
                println("yep")
            }
            if(level.getClimbableXY(x, y) != null)
                return climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.CLIMBABLE ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.CLIMBABLE_HOOP ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.ROPE_END_CLIMBABLE ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.CLIMBABLE_ROPE ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.VINE_START ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.VINE_1 ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.VINE_2 ||
                        climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.VINE_3
            return false
        }

        fun isEmptyHoop(x: Int, y: Int, level : GameLevel) : Boolean {
            if(level.getClimbableXY(x, y) != null)
                return climbableAttributes[level.getClimbableXY(x, y)]!! == ClimbableAttribute.EMPTY_HOOP
            return false
        }

        fun setToClimbableHoop(x : Int, y : Int, level : GameLevel) {
            for((key, value) in climbableAttributes) {
                if(value == ClimbableAttribute.CLIMBABLE_HOOP) {
                    level.setClimbableXY(x, y, key)
                    break
                }
            }
        }

        fun setToRopeEnd(x : Int, y : Int, level : GameLevel) {
            for((key, value) in climbableAttributes) {
                if(value == ClimbableAttribute.ROPE_END_CLIMBABLE) {
                    level.setClimbableXY(x, y, key)
                    break
                }
            }
        }

        fun setToRope(x : Int, y : Int, level : GameLevel) {
            for((key, value) in climbableAttributes) {
                if(value == ClimbableAttribute.CLIMBABLE_ROPE) {
                    level.setClimbableXY(x, y, key)
                    break
                }
            }
        }

        fun setVineStart(x : Int, y : Int, level : GameLevel) {
            for((key, value) in climbableAttributes) {
                if(value == ClimbableAttribute.VINE_START) {
                    level.setClimbableXY(x, y, key)
                    break
                }
            }
        }

        fun setVine(x : Int, y : Int, level : GameLevel) {
            randomNumForVine = Random.nextInt(0, 2)
            var vineAttribute = when (randomNumForVine) {
                0 -> {
                    ClimbableAttribute.VINE_1
                }
                1 -> {
                    ClimbableAttribute.VINE_2
                }
                else -> {
                    ClimbableAttribute.VINE_3
                }
            }

            for((key, value) in climbableAttributes) {
                if(value == vineAttribute) {
                    level.setClimbableXY(x, y, key)
                    break
                }
            }
        }
    }
}