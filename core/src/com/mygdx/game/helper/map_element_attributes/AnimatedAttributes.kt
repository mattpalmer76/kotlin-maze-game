package com.mygdx.game.helper.map_element_attributes

class AnimatedAttributes {
    public enum class AnimatedAttribute {
        EMPTY,
        FIRE,
        SPIDER
    }

    var animatedAttributes = mapOf<Int, AnimatedAttribute>(
            0 to AnimatedAttribute.EMPTY,
            1 to AnimatedAttribute.FIRE,
            2 to AnimatedAttribute.SPIDER,
            3 to AnimatedAttribute.EMPTY,
            4 to AnimatedAttribute.EMPTY
    )
}