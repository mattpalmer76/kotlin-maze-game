package com.mygdx.game.helper

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.mygdx.game.config.Constants
import com.mygdx.game.model.MovementTextureAtlantes

// all assets are stored here in the companion object for convenience
// I'm only using the same sets of graphics for the whole game
// more ambitious projects would need something way better than this...

class Assets {
    companion object {
        // store graphics assets here
        lateinit var blocksTA: TextureAtlas
        lateinit var climbablesTA: TextureAtlas
        lateinit var itemsTA: TextureAtlas
        lateinit var obstaclesTA: TextureAtlas

        // animated tiles
        lateinit var fire1TA : TextureAtlas
        lateinit var spiderTA : TextureAtlas
        lateinit var wallTorchTA : TextureAtlas
        lateinit var candleTA : TextureAtlas

        // TODO store sprite data here

        // player texture atlantes
        lateinit var playerIdleTA : TextureAtlas
        lateinit var playerUpTA : TextureAtlas
        lateinit var playerDownTA : TextureAtlas
        lateinit var playerLeftTA : TextureAtlas
        lateinit var playerRightTA : TextureAtlas
        lateinit var playerMTAs : MovementTextureAtlantes

        // enemy 1 texture atlantes
        lateinit var enemy1IdleTA : TextureAtlas
        lateinit var enemy1UpTA : TextureAtlas
        lateinit var enemy1DownTA : TextureAtlas
        lateinit var enemy1LeftTA : TextureAtlas
        lateinit var enemy1RightTA : TextureAtlas
        lateinit var enemy1MTAs : MovementTextureAtlantes

        // skin
        lateinit var skin : Skin

        // for convenience
        lateinit var blockList: MutableList<TextureRegion>
        lateinit var climbableList: MutableList<TextureRegion>
        lateinit var itemList: MutableList<TextureRegion>
        lateinit var obstacleList: MutableList<TextureRegion>
        lateinit var animList: MutableList<TextureAtlas>

        // where all the enemy textures are stored
        var enemyMTAList = mutableListOf<MovementTextureAtlantes>()

        // load the assets from the drive into memory (it's not much)
        fun getAssets() {
            blocksTA = TextureAtlas(Gdx.files.internal(Constants.BLOCKS_PATH))
            climbablesTA = TextureAtlas(Gdx.files.internal(Constants.CLIMBABLES_PATH))
            itemsTA = TextureAtlas(Gdx.files.internal(Constants.ITEMS_PATH))
            obstaclesTA = TextureAtlas(Gdx.files.internal(Constants.OBSTACLES_PATH))

            // load animations
            // (TODO perhaps add a waterfall or something?)
            fire1TA = TextureAtlas(Gdx.files.internal(Constants.FIRE1_ANIM_PATH))
            spiderTA = TextureAtlas(Gdx.files.internal(Constants.SPIDER_ANIM_PATH))
            wallTorchTA = TextureAtlas(Gdx.files.internal(Constants.WALLTORCH_ANIM_PATH))
            candleTA = TextureAtlas(Gdx.files.internal(Constants.CANDLE_ANIM_PATH))
            animList = mutableListOf(fire1TA, spiderTA, wallTorchTA, candleTA)

            blockList = mutableListOf<TextureRegion>()
            for (i in 0 until Constants.BLOCK_COUNT) {
                blockList.add(blocksTA.findRegion(Constants.BLOCK_NAME + i.toString().padStart(4, '0')))
            }

            climbableList = mutableListOf<TextureRegion>()
            for (i in 0 until Constants.CLIMBABLE_COUNT) {
                climbableList.add(climbablesTA.findRegion(Constants.CLIMBABLE_NAME + i.toString().padStart(4, '0')))
            }

            itemList = mutableListOf<TextureRegion>()
            for (i in 0 until Constants.ITEM_COUNT) {
                itemList.add(itemsTA.findRegion(Constants.ITEM_NAME + i.toString().padStart(4, '0')))
            }

            obstacleList = mutableListOf<TextureRegion>()
            for (i in 0 until Constants.OBSTACLE_COUNT) {
                obstacleList.add(obstaclesTA.findRegion(Constants.OBSTACLE_NAME + i.toString().padStart(4, '0')))
            }

            // TODO prep animation tiles in some way (probably a list of texture atlantes)

            // TODO load sprites

            // load player stuff
            playerIdleTA = TextureAtlas(Gdx.files.internal(Constants.PLAYER_IDLE_PATH))
            playerUpTA = TextureAtlas(Gdx.files.internal(Constants.PLAYER_UP_PATH))
            playerDownTA = TextureAtlas(Gdx.files.internal(Constants.PLAYER_DOWN_PATH))
            playerLeftTA = TextureAtlas(Gdx.files.internal(Constants.PLAYER_LEFT_PATH))
            playerRightTA = TextureAtlas(Gdx.files.internal(Constants.PLAYER_RIGHT_PATH))
            playerMTAs = MovementTextureAtlantes(
                    idle = com.mygdx.game.helper.Assets.Companion.playerIdleTA,
                    up = playerUpTA,
                    down = playerDownTA,
                    left = playerLeftTA,
                    right = playerRightTA,
                    randomiseIdle = true,
                    slowdownMultiplierIdle = 16
            )

            // load recent enemy sprite (the floating head thing)
            enemy1IdleTA = TextureAtlas(Gdx.files.internal(Constants.ENEMY_0_IDLE_PATH))
            enemy1UpTA = TextureAtlas(Gdx.files.internal(Constants.ENEMY_0_UP_PATH))
            enemy1DownTA = TextureAtlas(Gdx.files.internal(Constants.ENEMY_0_DOWN_PATH))
            enemy1LeftTA = TextureAtlas(Gdx.files.internal(Constants.ENEMY_0_LEFT_PATH))
            enemy1RightTA = TextureAtlas(Gdx.files.internal(Constants.ENEMY_0_RIGHT_PATH))
            enemy1MTAs = MovementTextureAtlantes(enemy1IdleTA, enemy1UpTA, enemy1DownTA, enemy1LeftTA, enemy1RightTA)

            // load enemies MTAs into mutable list
            enemyMTAList.add(enemy1MTAs);

            // load skin
            skin = Skin(Gdx.files.internal(Constants.SKIN_PATH))
        }

        // dispose of assets loaded
        fun dispose() {
            blocksTA.dispose()
            climbablesTA.dispose()
            itemsTA.dispose()
            obstaclesTA.dispose()

            fire1TA.dispose()
            wallTorchTA.dispose()
            spiderTA.dispose()
            candleTA.dispose()

            enemy1IdleTA.dispose()
            enemy1UpTA.dispose()
            enemy1DownTA.dispose()
            enemy1LeftTA.dispose()
            enemy1RightTA.dispose()
        }
    }
}