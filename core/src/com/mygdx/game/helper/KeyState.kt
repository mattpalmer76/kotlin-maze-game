package com.mygdx.game.helper

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

// class for measuring key press time and returning a signal
// will return only one signal on keypress if not 'machine-gunning'
// otherwise will mimic usual Windows terminal (or DOS for us oldies)
// keyboard behaviour where one is returned and after a pause a bunch
// of 'machine-gunned' keypresses are returned while holding down key

// hoping my description makes sense...
class KeyState(doMachineGun : Boolean = false, key : Int = Input.Keys.SPACE) {
    private var doMachineGun = doMachineGun
    private var key = key

    // probably don't need this
    enum class KeyReadState {
        OPEN,
        SINGLE_CLOSE,
        MACHINE_GUN
    }

    // how many frames a key is held down for (1/60th of a second)
    private var keyReadCount : Int = 0;
    private var state : KeyReadState = KeyReadState.OPEN
        get() = this.state

    // each call to this function will give appropriate response
    // if key is held down
    // otherwise state is cleared
    public fun isKeyActive(isKeyPressed : Boolean) : Boolean {
        if(isKeyPressed) {
            keyReadCount++;
            if(keyReadCount == 1) {
                state= KeyReadState.SINGLE_CLOSE
                return true
            } else if(keyReadCount >= 18) {
                if(doMachineGun) {
                    state = KeyReadState.MACHINE_GUN
                    return keyReadCount % 2 == 0
                } else {
                    return false;
                }
            } else {
                return false
            }
        }
        state = KeyReadState.OPEN
        keyReadCount = 0;
        return false
    }

    // check on initialised value
    public fun check() : Boolean {
        return isKeyActive(Gdx.input.isKeyPressed(key))
    }
}