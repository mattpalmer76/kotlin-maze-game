package com.mygdx.game.helper

class FileUtils {
    companion object {
        // inserts placeholders in text to represent common directory locations on a system
        fun encodeDirectory(directory : String) : String {
            var returnDir = ""
            if(directory.contains(GameFiles.baseDirectory))
                returnDir = directory.replace(GameFiles.baseDirectory, "{base}")
            return returnDir
        }

        // translates back into a full directory appropriate for the system this is on
        fun decodeDirectory(directory : String) : String {
            var returnDir = ""
            if(directory.contains("{base}"))
                returnDir = directory.replace("{base}", GameFiles.baseDirectory)
            return returnDir
        }
    }
}