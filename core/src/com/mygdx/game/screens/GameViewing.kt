package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20
import com.mygdx.game.DragonGame
import com.mygdx.game.actors.Player
import com.mygdx.game.classes.Viewing
import com.mygdx.game.config.Constants
import com.mygdx.game.draw.DrawGameLevel
import com.mygdx.game.helper.FileUtils
import com.mygdx.game.helper.GameFiles
import com.mygdx.game.helper.KeyState
import com.mygdx.game.model.GameLevel
import com.mygdx.game.model.Location
import org.json.JSONObject
import java.io.File

class GameViewing() : Viewing() {
    lateinit var game : DragonGame
    lateinit var level : GameLevel
    var spaceKey = KeyState(false, Input.Keys.SPACE)
    private var levelNum = 0
    set(value) {
        if(value >= Constants.NUM_OF_LEVELS) {
            // do win game
        }
        field = value
    }

    lateinit var drawGameLevel : DrawGameLevel

    lateinit var player : Player

    constructor(game : DragonGame) : this() {
        this.game = game
        var loadedLevel = false
        while(!loadedLevel) {
            if(!loadLevelByNumber(levelNum)) {
                levelNum++
            } else {
                loadedLevel = true
            }
        }
        player = Player(level.getStartXY())
        drawGameLevel = DrawGameLevel(level, player)

        x = drawGameLevel.centerPlayerCoordsX(player.location)
        y = drawGameLevel.centerPlayerCoordsY(player.location)
    }

    // todo create constructor for loading game

    var x = 0
    var y = 0

    override fun render(delta: Float) {
        // clear the screen before refresh
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        drawGameLevel.draw(x, y, delta)

        super.render(delta)
    }

    override fun handleKeys() {
        super.handleKeys()

        var shiftHeld = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)
        var sprintMultiplier = if(shiftHeld) 2 else 1

        when {
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> {
                if(player.moveLeft(level)) {
                    if(player.getOnscreenX(x) < Constants.PMA_X1) {
                        x -= Constants.SCALING * sprintMultiplier
                    }
                    if(shiftHeld)
                        player.moveLeft(level)
                }
            }
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> {
                if(player.moveRight(level)) {
                    if(player.getOnscreenX(x) > Constants.PMA_X2) {
                        x += Constants.SCALING * sprintMultiplier
                    }
                    if(shiftHeld)
                        player.moveRight(level)
                }
            }
            Gdx.input.isKeyPressed(Input.Keys.UP) -> {
                if(player.moveUp(level)) {
                    if(player.getOnscreenY(y) > Constants.PMA_Y2) {
                        y += Constants.SCALING * sprintMultiplier
                    }
                    if(shiftHeld)
                        player.moveUp(level)
                }
            }
            Gdx.input.isKeyPressed(Input.Keys.DOWN) -> {
                if(player.moveDown(level)) {
                    if(player.getOnscreenY(y) < Constants.PMA_Y1) {
                        y -= Constants.SCALING * sprintMultiplier
                    }
                    if(shiftHeld)
                        player.moveDown(level)
                }
            }
            else -> {
                player.beIdle()
            }
        }

        if(spaceKey.check()) {
            player.pickupOrDrop(level)
        }
    }

    private fun loadLevelByNumber(num : Int) : Boolean {
        var levelPath = FileUtils.decodeDirectory(GameFiles.levelSelection[num]!!)
        var levelFile = File(levelPath + Constants.MAP_EXTENSION)
        if(levelFile.exists()) {
            level = GameLevel(JSONObject(levelFile.readText()))
            return true
        } else {
            level = GameLevel()
            return false
        }
    }
}