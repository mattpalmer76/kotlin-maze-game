package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.mygdx.game.DragonGame
import com.mygdx.game.classes.Modal
import com.mygdx.game.classes.Viewing
import com.mygdx.game.classes.ui.LevelMenu
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.*
import com.mygdx.game.popups.FileDialogModal

// screen for selecting levels
class LevelSelectionViewing(var game: DragonGame) : Viewing() {
    lateinit var levelMenu : LevelMenu
    lateinit var levelList : MutableList<String>
    lateinit var levelModal : Modal

    var levelSelection = -1

    // file dialog modal for selecting a map
    private var fileDialogModal = FileDialogModal(
            originDir = GameFiles.levelDir.absolutePath,
            extensions = mutableListOf("map"),
            onSelection = { selectedLevelName: String, path: String ->
                // get the full level path, run through 'encoder' to replace local directory with placeholder
                var levelName = FileUtils.encodeDirectory(path + "\\" + selectedLevelName)

                // remove .map, we don't need it
                if(selectedLevelName.endsWith(".map"))
                    levelName = levelName.replace(".map", "")

                // store name and refresh menu contents
                GameFiles.levelSelection[levelSelection] = levelName
                levelMenu.menuItems = refreshLevelMenu()
                closeFileDialog()
            },
            onCancel = {
                closeFileDialog()
            },
            option = FileDialogModal.Option.CUSTOM,
            customButtonText = "Select"
    )

    override fun show() {
        GameFiles.loadLevelSelection()
        refreshLevelMenu()

        // level menu
        levelMenu = LevelMenu(levelList) { index: Int, string: String ->
            // on selection, open file dialog
            levelSelection = index
            fileDialogModal.clean()
            showModal(fileDialogModal)
        }
        levelMenu.menuItemsVisible = 10

        // make the modal object here so it can access stuff in this class
        levelModal = object : Modal("", Assets.skin) {
            var iAmInitialised = true

            var delKey = KeyState(false, Input.Keys.FORWARD_DEL)
            var backSpaceKey = KeyState(false, Input.Keys.BACKSPACE )
            var iKey = KeyState(false, Input.Keys.I)

            override fun createTable() {
                super.createTable()

                if(iAmInitialised) {
                    table = Table()
                    table.add(levelMenu.genTable())
                    table.row()
                    val label = Label("Enter or space to choose level\nI inserts and DEL deletes\nESC to save and leave.", Assets.skin)
                    label.wrap = true
                    label.setAlignment(Align.center)
                    table.add(label).padTop(32f)
                }
            }

            override fun handleKeys() {
                super.handleKeys()
                levelMenu.handleKeys()

                if(GlobalKeyStates.esc.check()) {
                    GameFiles.saveLevelSelection()
                    game.screen = HomeMenuViewing(game)
                }

                // insert a space
                if(iKey.check()) {
                    var selectedIndex = levelMenu.selectedIndex

                    for(i in Constants.NUM_OF_LEVELS - 2 downTo selectedIndex) {
                        GameFiles.levelSelection[i + 1] = GameFiles.levelSelection[i]!!
                    }
                    GameFiles.levelSelection[selectedIndex] = ""

                    levelMenu.menuItems = refreshLevelMenu()
                }
                // delete a space
                if(delKey.check() || backSpaceKey.check()) {
                    var selectedIndex = levelMenu.selectedIndex

                    for(i in selectedIndex until Constants.NUM_OF_LEVELS - 1) {
                        GameFiles.levelSelection[i] = GameFiles.levelSelection[i + 1]!!
                        GameFiles.levelSelection[Constants.NUM_OF_LEVELS - 1] = ""
                    }

                    levelMenu.menuItems = refreshLevelMenu()
                }
            }
        }
        levelModal.alwaysRefreshTable = true // todo do we need this?
        levelModal.x = (Constants.SCREEN_WIDTH.toFloat() - levelModal.width) / 2f
        levelModal.y = (Constants.SCREEN_HEIGHT.toFloat() - levelModal.height) / 2f
        addOverlay(levelModal)
        showOverlay(levelModal)

        fileDialogModal.x = (Constants.Companion.SCREEN_WIDTH - fileDialogModal.modalWidth) / 2f
        fileDialogModal.y = 136f
        addModal(fileDialogModal)
    }

    override fun render(delta: Float) {
        // clear the screen before refresh
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        super.render(delta)
    }

    // refreshes the list used by the level menu
    private fun refreshLevelMenu() : MutableList<String> {
        levelList = mutableListOf<String>()

        for(i in 0 until Constants.NUM_OF_LEVELS) {
            var levelListItem = GameFiles.levelSelection[i]

            var itemsSplitByPath = levelListItem?.split('\\')

            // use the last part of the string as it is the name of the level
            var lastItem = itemsSplitByPath?.get(itemsSplitByPath.size - 1)

            levelList.add(lastItem!!)
        }

        return levelList
    }

    // put out here as it couldn't be in the callback function
    private fun closeFileDialog() {
        hideModal(fileDialogModal)
    }
}