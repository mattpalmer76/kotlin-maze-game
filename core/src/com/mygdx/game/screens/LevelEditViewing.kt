package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.mygdx.game.DragonGame
import com.mygdx.game.classes.Viewing
import com.mygdx.game.config.Constants
import com.mygdx.game.draw.DrawEditorLevel
import com.mygdx.game.helper.GameFiles
import com.mygdx.game.helper.GlobalKeyStates
import com.mygdx.game.helper.KeyState
import com.mygdx.game.model.EnemyLocation
import com.mygdx.game.model.Level
import com.mygdx.game.model.RectCoords
import com.mygdx.game.popups.*
import ktx.graphics.use
import net.spookygames.gdx.nativefilechooser.NativeFileChooserConfiguration
import org.json.JSONObject
import java.io.FilenameFilter

class LevelEditViewing(var game: DragonGame) : Viewing() {
    // for drawing
    private var levelBatch = SpriteBatch()
    private var levelCamera = OrthographicCamera().apply {
        setToOrtho(false, Constants.SCREEN_WIDTH.toFloat(), Constants.SCREEN_HEIGHT.toFloat())
    }

    // popups and modals
    private var quitPopup = TwoChoiceModal(
            "Are you sure you want to leave?",
            "Yes",
            "No",
            {
                game.screen = HomeMenuViewing(game);

            },
            {
                hidePopup(it)
            },
            false
    )
    private var overwritePopup = TwoChoiceModal(
            "Are you sure you want to overwrite that file?",
    "Yes",
            "No",
            {},
            {}
    )
    private var loadVerifyPopup = TwoChoiceModal(
            "Loading this file will overwrite any changes. Are you sure you want this?",
            "Yes",
            "No",
            {},
            {}
    )
    private var levelEditBarModal = LevelEditBarModal()
    private var helpModal = HelpModal()
    private var blockSelectorModal = BlockSelectorModal(
            { blockSelectorModal: BlockSelectorModal, index: Int, blockType: BlockSelectorModal.BlockType ->
                blockSelectorModal.display = false
                when(blockType) {
                    BlockSelectorModal.BlockType.ITEM -> {
                        levelEditBarModal.item = index
                    }
                    BlockSelectorModal.BlockType.CLIMBABLE -> {
                        levelEditBarModal.climbable = index
                    }
                    BlockSelectorModal.BlockType.OBSTACLE -> {
                        levelEditBarModal.obstacle = index
                    }
                    BlockSelectorModal.BlockType.ANIMATED -> {
                        // added this fix as on map, 0 is nothing wheres in anim list 0 is the first object (fire)
                        levelEditBarModal.animBlock = index + 1
                    }
                    BlockSelectorModal.BlockType.ENEMY -> {
                        levelEditBarModal.enemyIndex = index
                    }
                    else -> {
                        levelEditBarModal.block = index
                    }
                }
                levelEditBarModal.refreshTable()
            },
            {
                it.display = false
            }
    )
    private var fileDialogModal = FileDialogModal(
            originDir = GameFiles.levelDir.absolutePath,
            extensions = mutableListOf("map"),
            onSelection = { s: String, s1: String -> },
            onCancel = {}
    )
    private var fileNotFoundPopup = AlertModal("That file was not found.", "Fine", {})
    private var corruptPopup = AlertModal("That file is corrupt", "Damn", {})

    // for indicating dark zones
    private var shapeRenderer = ShapeRenderer()

    // are we drawing a dark area?
    private var isDrawingDarkArea = false

    // dark area coords
    private var x1DA = 0
    private var x2DA = 0
    private var y1DA = 0
    private var y2DA = 0

    // level stuff
    private lateinit var level: Level
    private lateinit var drawEditorLevel: DrawEditorLevel

    // position on level
    private var levelX: Int = 0
        private set(value) {
            field = when {
                value < -4 -> {
                    -4
                }
                value > Level.X_SIZE - (Constants.TILES_ACROSS - 5) -> {
                    (Level.X_SIZE - (Constants.TILES_ACROSS - 5))
                }
                else -> {
                    value
                }
            }
        }
    private var levelY: Int = 0
        private set(value) {
            field = when {
                value < -4 -> {
                    -4
                }
                value > Level.Y_SIZE - (Constants.TILES_UP - 5) -> {
                    Level.Y_SIZE - (Constants.TILES_UP - 5)
                }
                else -> {
                    value
                }
            }
        }

    // cursor and level positions
    // cursor and level view min/max logic handled in private setters
    private var cursorX: Int = 0
        private set (value) {
            when {
                value >= Constants.TILES_ACROSS-1 -> {
                    field = value - 1
                    levelX++
                }
                value < 0 -> {
                    field = value + 1
                    levelX--
                }
                else -> {
                    field = value
                }
            }
        }

    private var cursorY: Int = 8
        private set (value)  {
            when {
                value >= Constants.TILES_UP-1 -> {
                    field = value - 1
                    levelY++
                }
                value < 4 -> {
                    field = value + 1
                    levelY--
                }
                else -> {
                    field = value
                }
            }
        }

    // cursor image
    private var cursor : Texture = Texture(Gdx.files.internal("cursors/level_edit_cursor.png")) // todo put this in a constant
    private var cursorSprite : Sprite = Sprite(cursor)

    // for polling keyboard
    private var upKey = KeyState(true, Input.Keys.UP)
    private var downKey = KeyState(true, Input.Keys.DOWN)
    private var leftKey = KeyState(true, Input.Keys.LEFT)
    private var rightKey = KeyState(true, Input.Keys.RIGHT)
    private var delKey = KeyState(false, Input.Keys.FORWARD_DEL)
    private var sKey : KeyState = KeyState(false, Input.Keys.S)
    private var lKey : KeyState = KeyState(false, Input.Keys.L)
    private var dKey = KeyState(false, Input.Keys.D)
    private var rKey = KeyState(false, Input.Keys.R)
    private var hKey = KeyState(false, Input.Keys.H)
    private var xKey = KeyState(false, Input.Keys.X)
    private var fullStopKey = KeyState(false, Input.Keys.PERIOD)

    // saving and loading dialogs
    private var saveConf = NativeFileChooserConfiguration()
    private var loadConf = NativeFileChooserConfiguration()

    // for testing
    var testText: BitmapFont = BitmapFont();

    // run first before drawing frames
    override fun show() {
        // prep level
        if(!this::level.isInitialized)
            level = Level()

        // set up modals
        levelEditBarModal.setPosition(0f, 0f)
        levelEditBarModal.display = true
        addOverlay(levelEditBarModal)

        helpModal.setPosition(100f, 50f)
        addModal(helpModal)

        blockSelectorModal.setPosition(50f, 40f)
        addModal(blockSelectorModal)

        quitPopup.setPosition(275f, 225f)
        addPopup(quitPopup)

        overwritePopup.setPosition(275f, 225f)
        addPopup(overwritePopup)

        fileNotFoundPopup.setPosition(275f, 225f)
        addPopup(fileNotFoundPopup)

        corruptPopup.setPosition(275f, 225f)
        addPopup(corruptPopup)

        loadVerifyPopup.setPosition(275f, 185f)
        loadVerifyPopup.modalHeight = 275f
        addPopup(loadVerifyPopup)

        fileDialogModal.x = (Constants.Companion.SCREEN_WIDTH - fileDialogModal.modalWidth) / 2f
        fileDialogModal.y = 136f
        addModal(fileDialogModal)

        // for drawing a level
        drawEditorLevel = DrawEditorLevel(level)

        // set up saving and loading dialogs
        saveConf.directory = Gdx.files.absolute(System.getProperty("user.home"))
        saveConf.mimeFilter = "*"
        saveConf.title = "Save Level"
        saveConf.nameFilter = FilenameFilter { dir, name -> name.endsWith("lvl") } // todo why isn't this working? It should filter all .lvl

        loadConf.directory = Gdx.files.absolute(System.getProperty("user.home"))
        loadConf.title = "Load level"
        loadConf.mimeFilter = "*"
        loadConf.nameFilter = FilenameFilter { dir, name -> name.endsWith("lvl") }
    }

    // draw our stuff
    override fun render(delta: Float) {
        // clear the screen before refresh
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // level drawing
        levelCamera.update()

        // first of all draw the level
        drawEditorLevel.draw(levelX, levelY, delta)

        // this draws a box which encompasses the dark area bounds
        if(isDrawingDarkArea) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
            shapeRenderer.color = Color(0.1f, 0.5f, 1f, 1f)
            var rect = RectCoords(x1DA - levelX, y1DA - levelY, cursorX, cursorY)
            shapeRenderer.rect(
                    rect.x1.toFloat() * Constants.SCALED_TILE_WIDTH,
                    rect.y1.toFloat() * Constants.SCALED_TILE_HEIGHT,
                    (1 + rect.x2 - rect.x1) * Constants.SCALED_TILE_WIDTH.toFloat() - 1f,
                    (1 + rect.y2 - rect.y1) * Constants.SCALED_TILE_HEIGHT.toFloat() - 1f
            )
            shapeRenderer.end()
        }

        // draw the cursor icon
        levelBatch.use {
            // set 'cursor' square onscreen
            cursorSprite.setPosition(cursorX * Constants.SCALED_TILE_WIDTH.toFloat(), cursorY * Constants.SCALED_TILE_HEIGHT.toFloat())
            cursorSprite.setSize(Constants.SCALED_TILE_WIDTH.toFloat(), Constants.SCALED_TILE_HEIGHT.toFloat())
            cursorSprite.draw(levelBatch)
        }

        // modals and whatnot will be drawn now
        super.render(delta)
    }

    override fun dispose() {
        super.dispose()
        levelBatch.dispose()
    }

    override fun handleKeys() {
        super.handleKeys() // not necessary at present but here just in case

        // handle modal popups, one at a time
        when {
            hKey.check() -> {
                // get a help screen
                showModal(helpModal)
            }
            xKey.check() -> {
                // get select screen
                showModal(blockSelectorModal)
                blockSelectorModal.selectedBlock = when {
                    levelEditBarModal.itemsSelected -> {
                        BlockSelectorModal.BlockType.ITEM
                    }
                    levelEditBarModal.climbablesSelected -> {
                        BlockSelectorModal.BlockType.CLIMBABLE
                    }
                    levelEditBarModal.obstaclesSelected -> {
                        BlockSelectorModal.BlockType.OBSTACLE
                    }
                    levelEditBarModal.animatedSelected -> {
                        BlockSelectorModal.BlockType.ANIMATED
                    }
                    levelEditBarModal.enemySelected -> {
                        BlockSelectorModal.BlockType.ENEMY
                    }
                    else -> {
                        BlockSelectorModal.BlockType.BLOCK
                    }
                }

                blockSelectorModal.display = true
                blockSelectorModal.refreshTable()
            }
            GlobalKeyStates.esc.check() -> {
                showPopup(quitPopup)
                quitPopup.display = true
            }
        }

        // set player start
        if(fullStopKey.check()) {
            level.setStartXY(cursorX + levelX, cursorY + levelY)
        }

        // placing and deleting block
        var x = cursorX + levelX
        var y = cursorY + levelY
        if(GlobalKeyStates.space.check()) {
            when {
                levelEditBarModal.obstaclesSelected -> {
                    level.setObstacleXY(x, y, levelEditBarModal.obstacle)
                }
                levelEditBarModal.climbablesSelected -> {
                    level.setClimbableXY(x, y, levelEditBarModal.climbable)
                }
                levelEditBarModal.itemsSelected -> {
                    level.setItemXY(x, y, levelEditBarModal.item)
                }
                levelEditBarModal.blocksSelected -> {
                    level.setBlockXY(x, y, levelEditBarModal.block)
                }
                levelEditBarModal.animatedSelected -> {
                    level.setAnimXY(x, y, levelEditBarModal.animBlock)
                }
                levelEditBarModal.enemySelected -> {
                    level.addEnemy(EnemyLocation(levelEditBarModal.enemyIndex, x, y))
                    drawEditorLevel.prepEnemiesForDisplay()
                }
            }
        }
        if(delKey.check()){
            when {
                levelEditBarModal.obstaclesSelected -> {
                    level.setObstacleXY(x, y, 0)
                }
                levelEditBarModal.climbablesSelected -> {
                    level.setClimbableXY(x, y, 0)
                }
                levelEditBarModal.itemsSelected -> {
                    level.setItemXY(x, y, 0)
                }
                levelEditBarModal.blocksSelected -> {
                    level.setBlockXY(x, y, 0)
                }
                levelEditBarModal.animatedSelected -> {
                    level.setAnimXY(x, y, 0)
                }
                levelEditBarModal.enemySelected -> {
                    level.deleteEnemy(x, y)
                    drawEditorLevel.prepEnemiesForDisplay()
                }
            }
        }

        // saving and loading
        if(sKey.check()) {
            fileDialogModal.onSelection = { s: String, s1: String ->
                var fileName = s1 + "\\" + s
                if(!fileName.endsWith(Constants.MAP_EXTENSION))
                    fileName += Constants.MAP_EXTENSION
                var file = FileHandle(fileName)
                if(file.exists()) {
                    overwritePopup.callbackOne = {
                        level.levelX = levelX
                        level.levelY = levelY
                        file.writeString(level.getJSONObject().toString(), false)
                    }
                    showPopup(overwritePopup)
                } else {
                    level.levelX = levelX
                    level.levelY = levelY
                    file.writeString(level.getJSONObject().toString(), false)
                }
            }
            fileDialogModal.option = FileDialogModal.Option.SAVE
            fileDialogModal.clean()
            showModal(fileDialogModal)
        }
        if(lKey.check()) {
            fileDialogModal.onSelection = { s: String, s1: String ->
                var fileName = s1 + "\\" + s
                if(!fileName.endsWith(Constants.MAP_EXTENSION))
                    fileName += Constants.MAP_EXTENSION
                var file = FileHandle(fileName)

                var doesFileExist = file != null && file.exists()
                if(doesFileExist) {
                    loadVerifyPopup.callbackOne = {
                        try {
                            var data = JSONObject(file.readString())
                            level = Level(data)
                            levelX = level.levelX
                            levelY = level.levelY
                            show()

                            fileDialogModal.clean()
                        } catch(e : Exception) {
                            showPopup(corruptPopup)
                        }
                    }
                    showPopup(loadVerifyPopup)
                } else {
                    showPopup(fileNotFoundPopup)
                }
            }
            fileDialogModal.option = FileDialogModal.Option.LOAD
            fileDialogModal.clean()
            showModal(fileDialogModal)
        }

        // draw dark areas
        if(dKey.check()) {
            if(cursorX + levelX > -1 && cursorY + levelY > -1 && cursorX + levelX < Level.X_SIZE && cursorY + levelY < Level.Y_SIZE) {
                if (!isDrawingDarkArea) {
                    x1DA = cursorX + levelX
                    y1DA = cursorY + levelY
                    isDrawingDarkArea = true
                } else {
                    x2DA = cursorX + levelX
                    y2DA = cursorY + levelY
                    level.setDarkArea(RectCoords(x1DA, y1DA, x2DA, y2DA).keepVarsInLevelArea())
                    isDrawingDarkArea = false
                }
            }
        }

        // delete dark areas
        if(rKey.check()) {
            level.deleteDarkArea(cursorX + levelX, cursorY + levelY)
        }

        // do cursor keys
        if (upKey.check())
            if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT))
                levelY += 12
            else
                cursorY++
        if (downKey.check())
            if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT))
                levelY -= 12
            else
                cursorY--
        if (leftKey.check())
            if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT))
                levelX -= 12
            else
                cursorX--
        if (rightKey.check())
            if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT))
                levelX += 12
            else
                cursorX++
    }
}