package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.mygdx.game.DragonGame
import com.mygdx.game.classes.Viewing
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.popups.FileDialogModal
import com.mygdx.game.popups.TextMenuModal

class HomeMenuViewing(var game: DragonGame) : Viewing() {
    lateinit var textMenuModal: TextMenuModal

    var menuItems = mutableListOf<String>(
            "Play Game",
            "Load Game",
            "Assign Levels",
            "Edit Level",
            "Exit"
    )

    override fun show() {
        super.show()

        textMenuModal = TextMenuModal(
                menuItems,
                { i: Int, _: String ->
                    when (i) {
                        0 -> {
                            // game screen
                            game.screen = GameViewing(game)
                        }
                        2 -> {
                            // level select screen
                            game.screen = LevelSelectionViewing(game)
                        }
                        3 -> {
                            // load level editor for set screen
                            game.screen = LevelEditViewing(game)
                        }
                        4 -> {
                            Gdx.app.exit()
                        }
                        else -> {

                        }
                    }
                },
                "",
                Assets.skin,
                alignLeft = false,
                showSideBar = false
        )

        addPopup(textMenuModal)
        textMenuModal.display = true
        textMenuModal.x = (Constants.SCREEN_WIDTH - textMenuModal.modalWidth) / 2f
        textMenuModal.y = 100f

    }

    override fun render(delta: Float) {
        // clear the screen before refresh
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        
        super.render(delta)
    }
}