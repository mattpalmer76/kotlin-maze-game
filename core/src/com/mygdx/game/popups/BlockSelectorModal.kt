package com.mygdx.game.popups

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.mygdx.game.classes.Modal
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GlobalKeyStates
import com.mygdx.game.helper.KeyState
import kotlin.math.floor

class BlockSelectorModal(val selectCallback: (blockSelectorModal : BlockSelectorModal, index : Int, blockType : BlockType) -> Unit, val escCallback: (blockSelectorModal : BlockSelectorModal) -> Unit) : Modal("", Assets.skin) {
    private var columnCount = 8

    override var modalWidth = 700f
    override var modalHeight = 550f
    private var padding = 10f

    private var numberToDisplay = 0
    private var fullRows = 0
    private var partialRowSize = 0

    private var up = KeyState(false, Input.Keys.UP)
    private var down = KeyState(false, Input.Keys.DOWN)
    private var left = KeyState(false, Input.Keys.LEFT)
    private var right = KeyState(false, Input.Keys.RIGHT)

    var selectedIndex : Int = 0

    private var cursorPosX = 0
        set(value) {
            when {
                value < 0 -> {
                    if(cursorPosY <= 0) {
                        field = if(partialRowSize == 0) {
                            columnCount - 1
                        } else {
                            partialRowSize - 1
                        }
                        cursorPosY = fullRows
                    } else {
                        field = columnCount - 1
                        cursorPosY--
                    }
                }
                value >= partialRowSize && cursorPosY >= fullRows -> {
                    field = 0
                    cursorPosY = 0
                }
                value >= columnCount -> {
                    field = 0
                    cursorPosY++
                }
                else -> {
                    field = value
                }
            }
        refreshTable()
    }

    private var cursorPosY = 0
        set(value) {
            var highest = getCount() - getStartingBlockIndex() - 1
            if(fullRows == 0 || (fullRows == 1 && partialRowSize == 0)) {
                field = 0
            } else {
                when {
                    value < 0 -> {
                        field = fullRows
                        if((fullRows - 1) * columnCount + cursorPosX > highest) {
                            cursorPosX = highest
                        }
                    }
                    value >= fullRows && fullRows * columnCount + cursorPosX > highest -> {
                        cursorPosX = partialRowSize - 1
                        field = value
                    }
                    value > fullRows -> {
                        field = 0
                    }
                    else -> {
                        field = value
                    }
                }
            }
        refreshTable()
    }

    private var cursorImage = Image(Texture(Gdx.files.internal("cursors/level_edit_cursor.png")))

    override var display: Boolean
        get() = super.display
        set(value) {
            cursorPosX = 0
            cursorPosY = 0
            super.display = value
        }

    enum class BlockType {
        BLOCK,
        ITEM,
        CLIMBABLE,
        OBSTACLE,
        ANIMATED,
        ENEMY
    }

    var selectedBlock : BlockType = BlockType.BLOCK

    override fun draw(batch: Batch, parentAlpha: Float) {
        super.draw(batch, parentAlpha)

        // TODO cursor for selecting stuff
    }

    init {
        refreshTable()
    }

    // todo fix! class attributes aren't being set to values on instantiation! why?
    private fun whyDoIHaveToDoThis() {
        if(modalWidth == 0f)
            modalWidth = 600f
        if(modalHeight == 0f)
            modalHeight = 300f
        if(selectedBlock == null)
            selectedBlock = BlockType.BLOCK
        if(padding == 0f)
            padding = 10f
        columnCount = floor(modalWidth / (getScalingWidth() + 2f * padding)).toInt()
        if(cursorImage == null)
            cursorImage = Image(Texture(Gdx.files.internal("cursors/level_edit_cursor.png")))

        if(up == null)
            up = KeyState(false, Input.Keys.UP)
        if(down == null)
            down = KeyState(false, Input.Keys.DOWN)
        if(left == null)
            left = KeyState(false, Input.Keys.LEFT)
        if(right == null)
            right = KeyState(false, Input.Keys.RIGHT)
    }

    override fun createTable() {
        whyDoIHaveToDoThis()

        super.createTable()

        // prep for drawing all selected items in a table
        numberToDisplay = getCount() - getStartingBlockIndex()
        fullRows = (numberToDisplay - (numberToDisplay % columnCount)) / columnCount
        partialRowSize = numberToDisplay % columnCount

        table.width = modalWidth

        var indexFromCursor = (cursorPosY * columnCount) + cursorPosX

        // generate full rows first
        for(c in 0 until fullRows) {
            table.row().padBottom(padding)
            for(r in 0 until columnCount) {
                var index = (c * columnCount) + r
                var image = Image(getTRForType(index))
                var stack = Stack()
                stack.add(image)
                if(index == indexFromCursor)
                    stack.add(cursorImage)
                table.add(stack).padLeft(padding).padRight(padding).width(getScalingWidth()).height(getScalingHeight())
            }
        }
        if(partialRowSize > 0) {
            table.row().padBottom(padding)
            for(r in 0 until partialRowSize) {
                var index = fullRows * columnCount + r
                var image = Image(getTRForType(index))
                var stack = Stack()
                stack.add(image)
                if(index == indexFromCursor)
                    stack.add(cursorImage)
                table.add(stack).padLeft(padding).padRight(padding).width(getScalingWidth()).height(getScalingHeight())
            }
        }
    }

    override fun handleKeys() {
        super.handleKeys()

        if(GlobalKeyStates.esc.check()) {
            display = false
            selectedIndex = -1
            escCallback(this)
        } else if(GlobalKeyStates.space.check() || GlobalKeyStates.enter.check()) {
            selectedIndex = cursorPosY * columnCount + cursorPosX + getStartingBlockIndex()
            display = false
            selectCallback(this, selectedIndex, selectedBlock)
        }

        if(up.check())
            cursorPosY--
        if(down.check())
            cursorPosY++
        if(left.check())
            cursorPosX--
        if(right.check())
            cursorPosX++
    }

    private fun getTRForType(index : Int) : TextureRegion {
        var adjustedIndex = index + getStartingBlockIndex()
        return when (selectedBlock) {
            BlockType.ITEM -> {
                Assets.itemList[adjustedIndex]
            }
            BlockType.CLIMBABLE -> {
                Assets.climbableList[adjustedIndex]
            }
            BlockType.OBSTACLE -> {
                Assets.obstacleList[adjustedIndex]
            }
            BlockType.ANIMATED -> {
                Assets.animList[adjustedIndex].findRegion("0")
            }
            BlockType.ENEMY -> {
                Assets.enemyMTAList[adjustedIndex].idle.findRegion("0")
            }
            else -> {
                Assets.blockList[adjustedIndex]
            }
        }
    }

    // how many of the selected item type exist? get from constants
    private fun getCount() : Int {
        return when (selectedBlock) {
            BlockType.ITEM -> {
                Constants.ITEM_COUNT
            }
            BlockType.CLIMBABLE -> {
                Constants.CLIMBABLE_COUNT
            }
            BlockType.OBSTACLE -> {
                Constants.OBSTACLE_COUNT
            }
            BlockType.ANIMATED -> {
                Constants.ANIM_COUNT
            }
            BlockType.ENEMY -> {
                Constants.ENEMIES_COUNT
            }
            else -> Constants.BLOCK_COUNT
        }
    }

    // get proper scaling width
    private fun getScalingWidth() : Float {
        return when (selectedBlock) {
            BlockType.BLOCK -> {
                return Constants.SCALED_TILE_WIDTH * 1.5f
            }
            else -> {
                return Constants.SCALED_TILE_WIDTH.toFloat()
            }
        }
    }

    // get proper scaling height
    private fun getScalingHeight() : Float {
        return when (selectedBlock) {
            BlockType.BLOCK -> {
                return Constants.SCALED_TILE_HEIGHT * 1.5f
            }
            else -> {
                return Constants.SCALED_TILE_HEIGHT.toFloat()
            }
        }
    }

    // get starting count as most lists of things start with an empty at index zero
    private fun getStartingBlockIndex() : Int {
        return when (selectedBlock) {
            BlockType.ENEMY -> {
                return 0
            }
            BlockType.ANIMATED -> {
                return 0
            }
            else -> {
                return 1
            }
        }
    }
}