package com.mygdx.game.popups

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.mygdx.game.classes.Modal
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GlobalKeyStates

class HelpModal() : Modal("", Assets.skin) {
    override fun createTable() {
        super.createTable()

        // different headings for different text colours
        var headingLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        var contentLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        headingLabelStyle.fontColor = Color.WHITE

        // draw table
        table.row().padBottom(15f)
        table.add().width(100f)
        table.add(Label("Help", headingLabelStyle)).width(500f).align(Align.left)

        table.row()
        table.add(Label("1", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select blocks", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("2", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select items", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("3", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select climbables", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("4", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select obstacles", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("5", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select animated blocks", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("6", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select enemy", contentLabelStyle)).align(Align.left)

        table.row().padBottom(15f)
        table.add(Label("x", headingLabelStyle)).padLeft(30f)
        table.add(Label("Open item selection", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("+", headingLabelStyle)).padLeft(30f)
        table.add(Label("Increment selection", contentLabelStyle)).align(Align.left)

        table.row().padBottom(15f)
        table.add(Label("-", headingLabelStyle)).padLeft(30f)
        table.add(Label("Decrement selection", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("d", headingLabelStyle)).padLeft(30f)
        table.add(Label("Begin and end drawing dark", contentLabelStyle)).align(Align.left)

        table.row()
        table.add().padLeft(30f)
        table.add(Label("area box", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("r", headingLabelStyle)).padLeft(30f)
        table.add(Label("Remove dark area box", contentLabelStyle)).align(Align.left)

        table.row().padBottom(15f)
        table.add(Label(".", headingLabelStyle)).padLeft(30f)
        table.add(Label("Set player start location", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("l", headingLabelStyle)).padLeft(30f)
        table.add(Label("Load a map", contentLabelStyle)).align(Align.left)

        table.row().padBottom(15f)
        table.add(Label("s", headingLabelStyle)).padLeft(30f)
        table.add(Label("Save a map", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("e", headingLabelStyle)).padLeft(30f)
        table.add(Label("Level entry point", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("b", headingLabelStyle)).padLeft(30f)
        table.add(Label("Select level border", contentLabelStyle)).align(Align.left)

        table.row().padBottom(15f)
        table.add(Label("h", headingLabelStyle)).padLeft(30f)
        table.add(Label("This help screen", contentLabelStyle)).align(Align.left)

        table.row()
        table.add(Label("ESC", headingLabelStyle)).padLeft(30f)
        table.add(Label("Quit edit screen", contentLabelStyle)).align(Align.left)

        table.row()
        table.add().padLeft(30f)
        table.add(Label("(Also leave this popup)", contentLabelStyle)).align(Align.left)
    }

    // check key presses
    override fun handleKeys() {
        super.handleKeys()
        if(GlobalKeyStates.esc.check()) {
            display = false
        }
    }
}