package com.mygdx.game.popups

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.mygdx.game.classes.Modal
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GlobalKeyStates

class AlertModal(
        var message : String,
        var buttonText : String,
        var callback : () -> Unit
) : Modal("", Assets.skin) {
    override var modalWidth = 250f
    override var modalHeight = 200f

    override fun createTable() {
        super.createTable()

        alwaysRefreshTable = true;

        // different headings for different text colours
        var buttonLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        buttonLabelStyle.fontColor = Color.WHITE
        var messageLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        messageLabelStyle.fontColor = Color.GOLD

        table.row().padBottom(15f)

        var messageLabel = Label(message, messageLabelStyle)

        messageLabel.wrap = true
        messageLabel.setAlignment(Align.center)

        table.add(messageLabel).width(this.modalWidth - 64f)

        table.row().padBottom(15f)

        var label = Label(buttonText, buttonLabelStyle)
        label.setAlignment(Align.center)
        table.add(label)
    }

    override fun handleKeys() {
        super.handleKeys()

        if(GlobalKeyStates.enter.check() || GlobalKeyStates.esc.check()) {
            display = false;
            callback();
        }
    }
}