package com.mygdx.game.popups

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.mygdx.game.classes.Modal
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.KeyState

class LevelEditBarModal() : Modal("", Assets.skin) {
    override var display = true
    override var alwaysRefreshTable = true
    override var modalWidth = Constants.SCREEN_WIDTH.toFloat()
    override var modalHeight = 120f

    private var plusKey = KeyState(true, Input.Keys.EQUALS)
    private var minusKey = KeyState(true, Input.Keys.MINUS)

    private var oneKey = KeyState(false, Input.Keys.NUM_1)
    private var twoKey = KeyState(false, Input.Keys.NUM_2)
    private var threeKey = KeyState(false, Input.Keys.NUM_3)
    private var fourKey = KeyState(false, Input.Keys.NUM_4)
    private var fiveKey = KeyState(false, Input.Keys.NUM_5)
    private var sixKey = KeyState(false, Input.Keys.NUM_6)

    // block number to display
    var block : Int = 1
        set(value) {
            field = when {
                value < 1 -> Constants.BLOCK_COUNT - 1
                value >= Constants.BLOCK_COUNT -> 1
                else -> value
            }
        }

    // item number to display
    var item : Int = 1
        set(value) {
            field = when {
                value < 1 -> Constants.ITEM_COUNT - 1
                value >= Constants.ITEM_COUNT -> 1
                else -> value
            }
        }

    // climbable number to display (I realise climbable probably isn't a word)
    var climbable : Int = 1
        set(value) {
            field = when {
                value < 1 -> Constants.CLIMBABLE_COUNT - 1
                value >= Constants.CLIMBABLE_COUNT -> 1
                else -> value
            }
        }

    // obstacle number to display
    var obstacle : Int = 1
        set(value) {
            field = when {
                value < 1 -> Constants.OBSTACLE_COUNT - 1
                value >= Constants.OBSTACLE_COUNT -> 1
                else -> value
            }
        }

    // animated block to display
    var animBlock : Int = 1
        set(value) {
            field = when {
                value < 1 -> Constants.ANIM_COUNT
                value > Constants.ANIM_COUNT -> 1
                else -> value
            }
        }

    var enemyIndex : Int = 0
        set(value) {
            field = when {
                value < 0 -> Constants.ENEMIES_COUNT - 1
                value >= Constants.ANIM_COUNT -> 0
                else -> value
            }
        }

    // select blocks...
    var blocksSelected : Boolean = true
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    // select boolean...
    var itemsSelected : Boolean = false
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    // select climbable to edit with
    var climbablesSelected : Boolean = false
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    // select obstacles...
    var obstaclesSelected : Boolean = false
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    // select animated blocks...
    var animatedSelected : Boolean = false
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    // select enemy...
    var enemySelected : Boolean = false
        set(value) {
            field = if(value) {
                clearAllSelectionBools()
                true
            } else {
                false
            }
            refreshTable()
        }

    override fun createTable() {
        super.createTable()

        // different title styles (colours) to indicate what type of block is being selected
        var normalLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        var selectLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        selectLabelStyle.fontColor = Color.WHITE

        var labelStyle = normalLabelStyle

        labelStyle = if(blocksSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Block", labelStyle)).padLeft(5f).padRight(5f)

        labelStyle = if(itemsSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Item", labelStyle)).padLeft(5f).padRight(5f)

        labelStyle = if(climbablesSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Climber", labelStyle)).padLeft(5f).padRight(5f)

        labelStyle = if(obstaclesSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Obstacle", labelStyle)).padLeft(5f).padRight(5f)

        labelStyle = if(animatedSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Animated", labelStyle)).padLeft(5f).padRight(5f)

        labelStyle = if(enemySelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("Enemy", labelStyle)).padLeft(5f).padRight(5f)

        table.add();

        table.row()

        table.add(Image(Assets.blockList[block]))
        table.add(Image(Assets.itemList[item]))
        table.add(Image(Assets.climbableList[climbable]))
        table.add(Image(Assets.obstacleList[obstacle]))

        if(animBlock > 0) {
            table.add(Image(Assets.animList[animBlock - 1].findRegion("1")))
        }
        table.add(Image(Assets.enemyMTAList[enemyIndex].idle.findRegion("0")))

        table.row()

        labelStyle = if(blocksSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(1)", labelStyle))

        labelStyle = if(itemsSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(2)", labelStyle))

        labelStyle = if(climbablesSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(3)", labelStyle))

        labelStyle = if(obstaclesSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(4)", labelStyle))

        labelStyle = if(animatedSelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(5)", labelStyle))

        labelStyle = if(enemySelected)
            selectLabelStyle
        else
            normalLabelStyle
        table.add(Label("(6)", labelStyle))

        table.add(Label("(H)elp", normalLabelStyle))
    }

    // clear all selections of block types (so only one is selected at a time)
    private fun clearAllSelectionBools() {
        blocksSelected = false;
        itemsSelected = false;
        climbablesSelected = false;
        obstaclesSelected = false;
        animatedSelected = false;
        enemySelected = false;
    }

    // increment a selected block type
    fun incrementSelected() {
        if(blocksSelected) {
            block++
        }
        if(itemsSelected) {
            item++
        }
        if(climbablesSelected) {
            climbable++
        }
        if(obstaclesSelected) {
            obstacle++
        }
        if(animatedSelected) {
            animBlock++
        }

        refreshTable()
    }

    // decrement a selected block type
    fun decrementSelected() {
        if(blocksSelected) {
            block--
        }
        if(itemsSelected) {
            item--
        }
        if(climbablesSelected) {
            climbable--
        }
        if(obstaclesSelected) {
            obstacle--
        }
        if(animatedSelected) {
            animBlock--
        }
        refreshTable()
    }

    override fun handleKeys() {
        super.handleKeys()

        // do plus and minus for selecting tile
        if (plusKey.check())
            incrementSelected()
        if (minusKey.check())
            decrementSelected()

        // number keys for selecting which blocks to use
        if (oneKey.check())
            blocksSelected = true
        if (twoKey.check())
            itemsSelected = true
        if (threeKey.check())
            climbablesSelected = true
        if (fourKey.check())
            obstaclesSelected = true
        if (fiveKey.check())
            animatedSelected = true
        if (sixKey.check())
            enemySelected = true
    }
}