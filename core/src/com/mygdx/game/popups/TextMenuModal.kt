package com.mygdx.game.popups

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.mygdx.game.classes.Modal
import com.mygdx.game.classes.ui.TextMenu

open class TextMenuModal(
        var menuItems : MutableList<String>,
        val selection : (index : Int, selectedString : String) -> Unit,
        title : String,
        skin : Skin,
        alignLeft : Boolean = false,
        showSideBar : Boolean = false
    ) : Modal(title, skin) {

    override var modalWidth = 400f
    override var modalHeight = 160f

    override var alwaysRefreshTable = true

    private var textMenu = TextMenu(menuItems, selection, alignLeft, showSideBar)

    init {
        textMenu.menuWidth = modalWidth - 64f // 64 is four character spaces giving 'margins' of 32 pixels each side
    }

    // for some reason overridden methods are called before above variables initialised
    // this is my current work-around
    var iAmInitialised = true

    override fun createTable() {
        super.createTable()

        if(iAmInitialised) {
            table = textMenu.genTable()
        }
    }

    override fun handleKeys() {
        super.handleKeys()

        if(iAmInitialised) {
            textMenu.handleKeys()
        }
    }
}