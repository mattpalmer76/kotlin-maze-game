package com.mygdx.game.popups

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.mygdx.game.classes.Modal
import com.mygdx.game.helper.Assets
import java.io.File
import java.io.FilenameFilter
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle
import com.mygdx.game.classes.ui.FileMenu
import com.mygdx.game.helper.GlobalKeyStates
import com.mygdx.game.helper.KeyState

// dialog for file handling
class FileDialogModal(
        var originDir : String,
        var extensions : MutableList<String>,
        var onSelection : (
            fileName : String,
            dir : String
        ) -> Unit,
        var onCancel : (
            message : String
        ) -> Unit,
        var option : Option = Option.LOAD,
        var customButtonText : String = ""
    ) : Modal("", Assets.skin), InputProcessor {

    // custom allows for a custom button text
    enum class Option {
        LOAD,
        SAVE,
        CUSTOM
    }

    override var modalWidth = 750f
    override var modalHeight = 450f

    override var alwaysRefreshTable = true

    private var fileListing = mutableListOf<String>()
    private var dirListing = mutableListOf<String>()
    private var rootListing = mutableListOf<String>()
    private lateinit var fileMenu : FileMenu

    // for some reason overridden methods are called before above variables initialised
    // this is my current work-around
    var iAmInitialised = true

    lateinit var selectButton : TextButton
    var cancelButton = TextButton("Cancel", Assets.skin, "default")
    var textField = TextField("", Assets.skin, "default")

    // indicates which component of dialog is 'highlighted'
    var tab = 0
    set(value) {
        field = when {
            value < 0 -> (value % 4) + 4
            value > 3 -> value % 4
            else -> value
        }

        // default and selected styles applied to relevant components of dialog
        fileMenu.selected = field == 0
        if(value == 1)
            textField.style = Assets.skin.get("selected", TextField.TextFieldStyle::class.java)
        else
            textField.style = Assets.skin.get("default", TextField.TextFieldStyle::class.java)
        if(value == 2)
            selectButton.style = Assets.skin.get("selected", TextButtonStyle::class.java)
        else
            selectButton.style = Assets.skin.get("default", TextButtonStyle::class.java)
        if(value == 3)
            cancelButton.style = Assets.skin.get("selected", TextButtonStyle::class.java)
        else
            cancelButton.style = Assets.skin.get("default", TextButtonStyle::class.java)

    }

    init {
        setButtonText()
        loadDirContents()
        setUpFileMenu()

        Gdx.input.inputProcessor = this;
    }

    override fun createTable() {
        super.createTable()

        if(iAmInitialised) {
            table = Table()
            table.row()
            table.add(fileMenu.genTable())
            table.row()
            // filename
            table.add(textField).expand().fill().padLeft(64f).padRight(64f)
            table.row()

            var subTable = Table();
            subTable.row()
            selectButton.isChecked = true
            subTable.add(selectButton)
            subTable.add(cancelButton)
            table.add(subTable)
        }
    }

    // gets a directory listing of provided file extensions and returns as an array of strings
    private fun getDirectoryListing() : Array<String> {
        var file = File(originDir)

        val filenameFilter = object : FilenameFilter {
            override fun accept(dir: File, name: String): Boolean {
                for(extension in extensions) {
                    if(name.endsWith(extension)) {
                        return true
                    }
                }
                return false
            }
        }

        return try {
            file.list(filenameFilter)
        } catch(e : Exception) {
            arrayOf<String>()
        }

    }

    // gets a list of directories in a directory
    private fun getDirsInDirectory() : Array<String> {
        var file = File(originDir)

        return try {
            file.list { dir, name -> File(dir, name).isDirectory }
        } catch(e : Exception) {
            arrayOf<String>()
        }
    }

    // gets a list of 'roots', that is drives
    private fun getRoots() : Array<String> {
        var paths = File.listRoots()

        var pathMutableList = mutableListOf<String>()

        for(path in paths) {
            if(path != null) {
                pathMutableList.add(path.absolutePath)
            }
        }

        return pathMutableList.toTypedArray()
    }

    var tabKey = KeyState(false, Input.Keys.TAB)
    var backSpaceKey = KeyState(true, Input.Keys.BACKSPACE)
    var upKey = KeyState(false, Input.Keys.UP)
    var downKey = KeyState(false, Input.Keys.DOWN)

    // use arrow keys, select etc...
    override fun handleKeys() {
        super.handleKeys()

        if(iAmInitialised) {
            // esc cancels dialog
            if(GlobalKeyStates.esc.check()) {
                this.display = false
                onCancel("")
            }

            // change tab
            if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) {
                if(tabKey.check()) {
                    tab--
                }
            } else {
                if(tabKey.check()) {
                    tab++
                }
            }

            // text menu
            if(tab == 1) {
                if(backSpaceKey.check()) {
                    if(textField.text.isNotEmpty()) {
                        textField.text = textField.text.substring(0, textField.text.length - 1)
                    }
                }
                if(GlobalKeyStates.enter.check()) {
                    this.display = false
                    onSelection(textField.text, originDir)
                }
            }

            // text field
            if(tab == 2) {
                if(GlobalKeyStates.enter.check() || GlobalKeyStates.space.check()) {
                    this.display = false
                    onSelection(textField.text, originDir)
                }
            }

            // 'action' field
            if(tab == 3) {
                if(GlobalKeyStates.enter.check() || GlobalKeyStates.space.check()) {
                    this.display = false
                    onCancel("")
                }
            }

            // cancel field
            if(tab == 0 || tab == 2) {
                fileMenu.handleKeys()
                if(upKey.check() || downKey.check())
                    tab = 0
            }
        }
    }

    // get file contents of a directory
    private fun loadDirContents() {
        fileListing.clear()
        dirListing.clear()
        rootListing.clear()

        var rootsListingArray = getRoots()

        var fileListingArray = getDirectoryListing()
        for(string in fileListingArray) {
            fileListing.add(string)
        }

        var dirListingArray = getDirsInDirectory()

        var canAddRoot = false
        try {
            var testFile = File(originDir).parentFile
            if(testFile != null)
                dirListing.add("..")
            else {
                canAddRoot = true
            }
        } catch (e : Exception) {
            canAddRoot = true
        }

        for(string in dirListingArray) {
            dirListing.add(string)
        }

        if(canAddRoot) {
            for(string in rootsListingArray) {
                rootListing.add(string)
            }
        }
    }

    // create the file menu with drives, dirs and file listing
    private fun setUpFileMenu() {
        fileMenu = FileMenu(rootListing, dirListing, fileListing) { _: Int, s: String ->
            when {
                fileMenu.checkIfRoot(s) -> {
                    // when root
                    originDir = File(s).absolutePath
                    loadDirContents()
                    fileMenu.refresh(rootListing, dirListing, fileListing)
                    createTable()
                }
                fileMenu.checkIfDirectory(s) -> {
                    // if hit .. and not root directory, go back one
                    originDir = if(s == "..") {
                        File(originDir).parentFile.absolutePath;
                    } else {
                        originDir + "\\" + s
                    }
                    loadDirContents()
                    fileMenu.refresh(rootListing, dirListing, fileListing)
                    createTable()
                }
                else -> {
                    // put text into text field if it's a file
                    textField.text = s
                    tab = 2
                }
            }
        }
        fileMenu.menuItemsVisible = 10
        fileMenu.menuWidth = modalWidth - 128f
    }

    // clean up file dialog for next use
    fun clean() {
        tab = 0
        fileMenu.selectedIndex = 0
        textField.text = ""
        setButtonText()
        loadDirContents()
        fileMenu.refresh(rootListing, dirListing, fileListing)
        createTable()
    }

    // set the button text based on enum
    private fun setButtonText() {
        selectButton = when (option) {
            Option.LOAD -> TextButton("Load", Assets.skin, "default")
            Option.SAVE -> TextButton("Save", Assets.skin, "default")
            Option.CUSTOM -> TextButton(customButtonText, Assets.skin, "default")
            else -> TextButton("Load", Assets.skin, "default")
        }
    }

    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    // for typing characters into field
    override fun keyTyped(character: Char): Boolean {
        if(isVisible == true) {
            if(tab == 1) {
                textField.text += character
            } else {
                if(character in 'A'..'Z' || character in 'a'..'z' || character in '1'..'0' || character in '!'..')' || character == '_') {
                    tab = 1
                    textField.text += character
                }
            }
        }
        return true
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun scrolled(amount: Int): Boolean {
        return false
    }
}