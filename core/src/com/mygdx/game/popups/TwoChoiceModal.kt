package com.mygdx.game.popups

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.mygdx.game.classes.Modal
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GlobalKeyStates
import com.mygdx.game.helper.KeyState

class TwoChoiceModal(
        var message : String,
        var optionOne : String,
        var optionTwo : String,
        var callbackOne: (twoChoiceModal : TwoChoiceModal) -> Unit,
        var callbackTwo: (twoChoiceModal : TwoChoiceModal) -> Unit,
        var firstChoiceSelected : Boolean = true
    ) : Modal("", Assets.skin) {

    // left and right keys toggle which item is selected
    var left = KeyState(false, Input.Keys.LEFT)
    var right = KeyState(false, Input.Keys.RIGHT)

    override var modalWidth = 250f
    override var modalHeight = 200f

    override fun createTable() {
        super.createTable()

        // not sure this would work outside this block - funny business going on here
        alwaysRefreshTable = true

        // different headings for different text colours
        var headingLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        var contentLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        headingLabelStyle.fontColor = Color.WHITE

        table.row().padBottom(15f)

        var messageLabel = Label(message, headingLabelStyle)

        messageLabel.wrap = true
        messageLabel.setAlignment(Align.center)

        table.add(messageLabel).width(this.modalWidth - 64f)

        table.row().padBottom(15f)

        var subTable = Table()
        subTable.width = this.modalWidth - 64f

        subTable.row()

        var oneLabelStyle = headingLabelStyle
        var twoLabelStyle = contentLabelStyle

        if(!firstChoiceSelected) {
            twoLabelStyle = headingLabelStyle
            oneLabelStyle = contentLabelStyle
        }

        var oneLabel = Label(optionOne, oneLabelStyle)
        oneLabel.setAlignment(Align.center)
        subTable.add(oneLabel).width(subTable.width/2f)

        var twoLabel = Label(optionTwo, twoLabelStyle)
        twoLabel.setAlignment(Align.center)
        subTable.add(twoLabel).width(subTable.width/2f)

        table.add(subTable)
    }

    override fun handleKeys() {
        super.handleKeys()

        // toggle selections
        if(left.check() || right.check())
            firstChoiceSelected = !firstChoiceSelected

        // stop displaying and use callback
        if(GlobalKeyStates.esc.check()) {
            display = false
            this.callbackTwo(this)
        }

        // stop displaying and use callbacks
        if(GlobalKeyStates.enter.check()) {
            display = false
            if(firstChoiceSelected) {
                this.callbackOne(this)
            } else {
                this.callbackTwo(this)
            }
        }
    }
}