package com.mygdx.game.draw

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.mygdx.game.actors.Being
import com.mygdx.game.actors.Enemy
import com.mygdx.game.model.Level
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.model.Location
import ktx.graphics.use
import java.awt.Shape

// class for drawing the given level
class DrawEditorLevel(
        val level : Level
) {
    init {
        prepEnemiesForDisplay()
    }
    // batch
    private var batch = SpriteBatch()

    // for drawing rectangles indicating unlit areas
    private var shapeRenderer = ShapeRenderer()

    // our hero
    private var player = Being(Assets.playerMTAs, Location(level.startX, level.startY))

    // list containing enemy assets to be displayed for the provided level
    private lateinit var enemies : MutableList<Enemy>

    // place this withing your draw batch to draw screen
    public fun draw(xPos : Int, yPos : Int, delta : Float) {
        setEnemyDrawLoc(xPos * Constants.SCALED_TILE_WIDTH, yPos * Constants.SCALED_TILE_HEIGHT)

        batch.use {
            for(x in Constants.TILES_ACROSS downTo -1) {
                for(y in -1 .. Constants.TILES_UP) {
                    // todo this could all be cleaned up with just two functions

                    // draw blocks (with offset)
                    if(level.getBlockXY(x + xPos, y + yPos) < Constants.BLOCK_COUNT)
                        batch.draw(
                                Assets.Companion.blockList[level.getBlockXY(x + xPos, y + yPos)],
                                x * Constants.SCALED_TILE_WIDTH.toFloat() - Constants.SCALED_TILE_WIDTH.toFloat()/2f,
                                y * Constants.SCALED_TILE_HEIGHT.toFloat(),
                                Constants.SCALED_TILE_WIDTH.toFloat() + Constants.SCALED_TILE_WIDTH.toFloat()/2f,
                                Constants.SCALED_TILE_HEIGHT.toFloat() + Constants.SCALED_TILE_HEIGHT.toFloat()/2f
                        )

                    // draw climbables
                    if(level.getClimbableXY(x + xPos, y + yPos) < Constants.CLIMBABLE_COUNT)
                        batch.draw(
                                Assets.climbableList[level.getClimbableXY(x + xPos, y + yPos)],
                                x * Constants.SCALED_TILE_WIDTH.toFloat(),
                                y * Constants.SCALED_TILE_HEIGHT.toFloat(),
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )

                    // draw obstacles
                    if(level.getObstacleXY(x + xPos, y + yPos) < Constants.OBSTACLE_COUNT)
                        batch.draw(
                                Assets.obstacleList[level.getObstacleXY(x + xPos, y + yPos)],
                                x * Constants.SCALED_TILE_WIDTH.toFloat(),
                                y * Constants.SCALED_TILE_HEIGHT.toFloat(),
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )

                    // draw items
                    if(level.getItemXY(x + xPos, y + yPos) < Constants.ITEM_COUNT)
                        batch.draw(
                                Assets.itemList[level.getItemXY(x + xPos, y + yPos)],
                                x * Constants.SCALED_TILE_WIDTH.toFloat(),
                                y * Constants.SCALED_TILE_HEIGHT.toFloat(),
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )

                    // draw animated blocks
                    if(level.getAnimBlockXY(x + xPos, y + yPos) != null) {
                        level.getAnimBlockXY(x + xPos, y + yPos)!!
                                .draw(x * Constants.SCALED_TILE_WIDTH.toFloat(),
                                        y * Constants.SCALED_TILE_HEIGHT.toFloat(),
                                        batch)
                    }

                }
            }
            // draw enemies
            for(enemy in enemies) {
                enemy.draw(batch, delta)
            }

            // draw player
            if(Location(level.startX, level.startY).checkCoordsInLevelBounds()) {
                player.spritePos(
                        (level.startX - xPos).toFloat() * Constants.SCALED_TILE_WIDTH,
                        (level.startY - yPos).toFloat() * Constants.SCALED_TILE_HEIGHT
                )
                player.draw(batch, delta)
            }
        }

        // draw dark area rectangles (only on edit screen)
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.color = Color(0.1f, 0.5f, 1f, 0.3f)
        for(rectCoords in level.darkAreasData) {
            shapeRenderer.rect(
                    (rectCoords.x1.toFloat() * Constants.SCALED_TILE_WIDTH) - (xPos * Constants.SCALED_TILE_WIDTH),
                    (rectCoords.y1.toFloat() * Constants.SCALED_TILE_HEIGHT) - (yPos * Constants.SCALED_TILE_HEIGHT),
                    (1 + rectCoords.x2 - rectCoords.x1) * Constants.SCALED_TILE_WIDTH.toFloat(),
                    (1 + rectCoords.y2 - rectCoords.y1) * Constants.SCALED_TILE_HEIGHT.toFloat()
            )
        }
        shapeRenderer.end()
        Gdx.gl.glDisable(GL20.GL_BLEND)
    }

    // prepares any enemy display data
    fun prepEnemiesForDisplay() {
        enemies = mutableListOf<Enemy>()
        for(enemyLocation in level.enemyData) {
            if(enemyLocation.isEnemyActive()) {
                var enemy = Enemy(Assets.enemyMTAList[enemyLocation.enemyIndex!!], enemyLocation)
                enemies.add(enemy)
            }
        }
    }

    // sets enemy draw location relative to portion of map displayed
    // note that x & y are pixel perfect
    private fun setEnemyDrawLoc(xPosOnMap : Int, yPosOnMap : Int) {
        for(enemy in enemies) {
            enemy.spritePos(enemy.xOnMap!! - xPosOnMap, enemy.yOnMap!! - yPosOnMap)
        }
    }

    public fun dispose()
    {
        batch.dispose()
    }
}