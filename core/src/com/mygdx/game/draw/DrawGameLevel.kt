package com.mygdx.game.draw

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.mygdx.game.actors.Player
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.model.GameLevel
import com.mygdx.game.model.Level
import com.mygdx.game.model.Location
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const
import ktx.graphics.use
import kotlin.math.log2

// note, positioning of player/enemies are at a pixel level, not a tile level
class DrawGameLevel(var gameLevel : GameLevel,
                    var player : Player) {
    companion object {
        private val POWER_TILE_WIDTH = log2(Constants.TILE_WIDTH.toFloat()).toInt() + 1
        private val POWER_TILE_HEIGHT = log2(Constants.TILE_HEIGHT.toFloat()).toInt() + 1
    }

    // batch
    private var batch = SpriteBatch()

    // draw level onscreen using pixel coordinates
    public fun draw(xPos : Int, yPos : Int, delta : Float) {
        var xPixel = xPos % Constants.SCALED_TILE_WIDTH
        if(xPixel < 0)
            xPixel += Constants.SCALED_TILE_WIDTH
        var yPixel = yPos % Constants.SCALED_TILE_HEIGHT
        if(yPixel < 0)
            yPixel += Constants.SCALED_TILE_HEIGHT
        var xTile = xPos shr POWER_TILE_WIDTH
        var yTile = yPos shr POWER_TILE_HEIGHT

        batch.use {
            for (x in Constants.TILES_ACROSS + 1 downTo -1) {
                for(y in -1 .. Constants.TILES_UP + 1) {
                    // todo this could all be cleaned up with just two functions

                    // draw blocks (with pixel level offset)
                    if (gameLevel.getBlockXY(x + xTile, y + yTile) < Constants.BLOCK_COUNT) {
                        batch.draw(
                                Assets.blockList[gameLevel.getBlockXY(x + xTile, y + yTile)],
                                (x * Constants.SCALED_TILE_WIDTH.toFloat() - Constants.SCALED_TILE_WIDTH.toFloat() / 2f) - xPixel,
                                (y * Constants.SCALED_TILE_HEIGHT.toFloat()) - yPixel,
                                Constants.SCALED_TILE_WIDTH.toFloat() + Constants.SCALED_TILE_WIDTH.toFloat() / 2f,
                                Constants.SCALED_TILE_HEIGHT.toFloat() + Constants.SCALED_TILE_HEIGHT.toFloat() / 2f
                        )
                    }

                    // draw climbables (with pixel level offset)
                    if (gameLevel.getClimbableXY(x + xTile, y + yTile) < Constants.CLIMBABLE_COUNT) {
                        batch.draw(
                                Assets.climbableList[gameLevel.getClimbableXY(x + xTile, y + yTile)],
                                (x * Constants.SCALED_TILE_WIDTH.toFloat()) - xPixel,
                                (y * Constants.SCALED_TILE_HEIGHT.toFloat()) - yPixel,
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )
                    }

                    // draw obstacles (with pixel level offset)
                    if (gameLevel.getObstacleXY(x + xTile, y + yTile) < Constants.OBSTACLE_COUNT) {
                        batch.draw(
                                Assets.obstacleList[gameLevel.getObstacleXY(x + xTile, y + yTile)],
                                (x * Constants.SCALED_TILE_WIDTH.toFloat()) - xPixel,
                                (y * Constants.SCALED_TILE_HEIGHT.toFloat()) - yPixel,
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )
                    }

                    // draw items (with pixel level offset)
                    if (gameLevel.getItemXY(x + xTile, y + yTile) < Constants.ITEM_COUNT) {
                        batch.draw(
                                Assets.itemList[gameLevel.getItemXY(x + xTile, y + yTile)],
                                (x * Constants.SCALED_TILE_WIDTH.toFloat()) - xPixel,
                                (y * Constants.SCALED_TILE_HEIGHT.toFloat()) - yPixel,
                                Constants.SCALED_TILE_WIDTH.toFloat(),
                                Constants.SCALED_TILE_HEIGHT.toFloat()
                        )
                    }

                    // draw animated blocks (with pixel level offset)
                    if(gameLevel.getAnimBlockXY(x + xTile, y + yTile) != null) {
                        gameLevel.getAnimBlockXY(x + xTile, y + yTile)!!
                                .draw(
                                        (x * Constants.SCALED_TILE_WIDTH.toFloat()) - xPixel,
                                        (y * Constants.SCALED_TILE_HEIGHT.toFloat()) - yPixel,
                                        batch)
                    }
                }
            }
            // todo call draw enemies

            // todo draw player

            player.setUpSpritePos(xPos, yPos)
            player.draw(batch, delta)
        }
    }

    public fun centerPlayerCoordsX(location : Location) : Int {
        if(location.x != null) {
            var x = when {
                location.x!! < 6 -> {
                    6
                }
                location.x!! > Level.X_SIZE - 7 -> {
                    Level.X_SIZE - 7
                }
                else -> {
                    location.x!!
                }
            }

            return x * Constants.SCALED_TILE_WIDTH - Constants.SCREEN_WIDTH / 2 + Constants.SCALED_TILE_WIDTH / 2
        }
        return 0
    }

    public fun centerPlayerCoordsY(location : Location) : Int {
        if(location.y != null) {
            var y = when {
                location.y!! < 6 -> {
                    6
                }
                location.y!! > Level.Y_SIZE - 7 -> {
                    Level.Y_SIZE - 7
                }
                else -> {
                    location.y!!
                }
            }

            return y * Constants.SCALED_TILE_HEIGHT - Constants.SCREEN_HEIGHT / 2 + Constants.SCALED_TILE_HEIGHT / 2
        }
        return 0
    }
}