package com.mygdx.game.classes.ui

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GlobalKeyStates
import com.mygdx.game.helper.KeyState
import kotlin.math.floor

open class TextMenu(
        var menuItems : MutableList<String>,
        val selection : (index : Int,
        selectedString : String) -> Unit,
        var alignLeft : Boolean = false,
        var showSideBar : Boolean = false
    ) : Actor() {

    companion object {
        const val SIDEBAR_SIZE = 16f
        private var maxChars = 0
    }

    // index of highlighted menu element
    var selectedIndex = 0
    set(value) {
        var temp = when{
            value >= menuItems.size - 1 -> {
                value % menuItems.size
            }
            value < 0 -> {
                (value % menuItems.size) + menuItems.size
            }
            else -> {
                value
            }
        }

        if(temp < visOptionsOffset)
            visOptionsOffset = temp
        if(temp >= visOptionsOffset + menuItemsVisible)
            visOptionsOffset = temp - menuItemsVisible + 1

        field = temp
    }

    // size of menu (0 or less uses defaults)
    var menuWidth : Float = 0f
    var menuHeight : Float = 0f

    // max amount of menu items to display
    var menuItemsVisible = 5

    // label styles
    var selectedLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
    var unselectedLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))

    var upKey = KeyState(false, Input.Keys.UP)
    var downKey = KeyState(false, Input.Keys.DOWN)
    var pgUpKey = KeyState(false, Input.Keys.PAGE_UP)
    var pgDownKey = KeyState(false, Input.Keys.PAGE_DOWN)

    private var visOptionsOffset = 0

    init {
        selectedLabelStyle.fontColor = Color.WHITE
    }

    var selected = true
    set(value) {
        if(value) {
            selectedLabelStyle.fontColor = Color.WHITE
        } else {
            selectedLabelStyle.fontColor = Color(0.8f, 0.8f, 0.8f, 1f)
        }
        field = value
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        super.draw(batch, parentAlpha) // not sure I need this

        genTable().draw(batch, parentAlpha)
    }

    // generate table from supplied text list
    public fun genTable() : Table {
        var table = Table()
        var subTable = Table()

        for(i in visOptionsOffset until visOptionsOffset + amountInVisArea()) {
            if(alignLeft) {
                subTable.row().width(menuWidth)
            } else {
                subTable.row()
            }
            menuElement(i).width = menuWidth
            subTable.add(menuElement(i))
        }

        table.row()

        if(showSideBar) {
            table.add(subTable).width(menuWidth - SIDEBAR_SIZE).align(Align.top)
            table.add(sideBar()).width(SIDEBAR_SIZE)
        } else {
            table.add(subTable)
        }

        if(menuWidth > 0f)
            table.width = menuWidth
        if(menuHeight > 0f)
            table.height= menuHeight

        return table
    }

    // calc how much is displayed in the visible area
    private fun amountInVisArea() : Int {
        return if(menuItems.size > menuItemsVisible) {
            menuItemsVisible
        } else {
            menuItems.size
        }
    }

    // make a simple sidebar
    open fun sideBar() : Stack {
        var stack = Stack()

        var sb = StringBuilder()

        for(i in 0 until menuItemsVisible) {
            var segmentSize = menuItems.size.toFloat() / menuItemsVisible.toFloat()
            var pos = floor(selectedIndex.toFloat() / segmentSize).toInt()
            if(i == pos)
                sb.append("*")
            else
                sb.append("|")
            if(i != menuItemsVisible - 1)
                sb.append("\n")
        }

        var label = Label(sb.toString(), unselectedLabelStyle)
        label.setAlignment(Align.center)
        stack.add(label)

        return stack
    }

    // how to draw each menu item
    open fun menuElement(index : Int) : Stack {
        var stack = Stack()

        var labelStyle = if(index == selectedIndex) {
            selectedLabelStyle
        } else {
            unselectedLabelStyle
        }

        var text = Label(menuItems[index].take(maxChars()), labelStyle)
        stack.add(text)

        return stack
    }

    // check keys
    open fun handleKeys() {
        var amountInVisArea = amountInVisArea()

        if(upKey.check())
            selectedIndex--
        if(downKey.check())
            selectedIndex++
        if(pgUpKey.check()) {
            if(visOptionsOffset - amountInVisArea > 0) {
                selectedIndex -= amountInVisArea
                visOptionsOffset = selectedIndex
            } else {
                if(selectedIndex - amountInVisArea > 0) {
                    selectedIndex -= amountInVisArea
                } else {
                    selectedIndex = 0
                }
                visOptionsOffset = 0
            }
        }
        if(pgDownKey.check()) {
            when {
                selectedIndex + amountInVisArea >= menuItems.size -> {
                    selectedIndex = menuItems.size - 1
                }
                selectedIndex + 2 * amountInVisArea >= menuItems.size -> {
                    selectedIndex = menuItems.size - amountInVisArea
                    visOptionsOffset = menuItems.size - amountInVisArea
                }
                else -> {
                    selectedIndex += amountInVisArea
                    visOptionsOffset += amountInVisArea - 1
                }
            }
        }

        if(GlobalKeyStates.space.check() || GlobalKeyStates.enter.check())
            selection(selectedIndex, menuItems[selectedIndex])
    }

    fun maxChars() : Int {
        if(maxChars == 0)
            maxChars = ((menuWidth - SIDEBAR_SIZE) / Constants.CHAR_WIDTH).toInt()
        return maxChars
    }
}