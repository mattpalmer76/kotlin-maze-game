package com.mygdx.game.classes.ui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.mygdx.game.helper.Assets

// a speial text menu tailored for a list of levels
class LevelMenu(menuItems: MutableList<String>, selection: (Int, String) -> Unit) : TextMenu(menuItems, selection) {
    // how to draw each menu item
    override fun menuElement(index : Int) : Stack {
        // use an extra style for <EMPTY> slots
        var emptyStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
        emptyStyle.fontColor = Color(0.4f, 0.4f, 0.6f, 1f)

        var stack = Stack()

        // normal styles
        var labelStyle = if(index == selectedIndex) {
            selectedLabelStyle
        } else {
            unselectedLabelStyle
        }

        // if it's empty, show <EMPTY> and if it's not selected, use emptyStyle
        var textFromMenu = menuItems[index].take(maxChars())
        if(textFromMenu.isEmpty()) {
            textFromMenu = "<EMPTY>"
            if(index != selectedIndex) {
                labelStyle = emptyStyle
            }
        }
        var text = Label(textFromMenu, labelStyle)

        // put it in a table so we can pad it
        var elementTable = Table()
        elementTable.add(text).padTop(8f).padBottom(8f)

        // add to stack and return
        stack.add(elementTable)
        return stack
    }
}
