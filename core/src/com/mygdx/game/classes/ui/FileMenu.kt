package com.mygdx.game.classes.ui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.mygdx.game.helper.Assets

// a text menu for files
// accepts drives (root items), dir items and file items - uses these so to know how to display types of each
open class FileMenu(
        var rootItems : MutableList<String>,
        var dirItems : MutableList<String>,
        var fileItems : MutableList<String>,
        selection : (index : Int,
             selectedString : String) -> Unit
    ) : TextMenu((rootItems + dirItems + fileItems) as MutableList<String>, selection, true, true) {

    // different styles for directories and drives
    var dirLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))
    var rootLabelStyle : Label.LabelStyle = Label.LabelStyle(Assets.skin.get(Label.LabelStyle::class.java))

    init {
        dirLabelStyle.fontColor = Color(0.3f, 0.6f, 1f, 1f)
        rootLabelStyle.fontColor = Color(0.1f, 0.8f, 1f, 1f)
    }

    fun refresh(
            rootItems: MutableList<String>,
            dirItems : MutableList<String>,
            fileItems : MutableList<String>
    ) {
        this.rootItems = rootItems
        this.dirItems = dirItems
        this.fileItems = fileItems
        this.menuItems = (rootItems + dirItems + fileItems) as MutableList<String>
        this.selectedIndex = 0
    }

    override fun menuElement(index: Int): Stack {
        var stack = Stack()

        // for checking selection type
        var isDirectory = checkIfDirectory(menuItems[index])
        var isRoot = checkIfRoot(menuItems[index])

        // different styles for different types
        var labelStyle = when {
            index == selectedIndex -> {
                selectedLabelStyle
            }
            isDirectory -> {
                dirLabelStyle
            }
            isRoot -> {
                rootLabelStyle
            }
            else -> {
                unselectedLabelStyle
            }
        }

        // different bracket styles for different types
        var text = when {
            isDirectory -> {
                Label("<" + menuItems[index].take(maxChars()) + ">", labelStyle)
            }
            isRoot -> {
                Label("[" + menuItems[index].take(maxChars()) + "]", labelStyle)
            }
            else -> {
                Label(menuItems[index].take(maxChars()), labelStyle)
            }
        }

        stack.add(text)

        return stack
    }

    // check to see whether a string is in the list of directories
    fun checkIfDirectory(name : String) : Boolean {
        return dirItems.contains(name)
    }

    // check to see whether a string is in the list of drives
    fun checkIfRoot(name : String) : Boolean {
        return rootItems.contains(name)
    }
}