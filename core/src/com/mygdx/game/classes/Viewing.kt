package com.mygdx.game.classes

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.mygdx.game.config.Constants
import ktx.graphics.use

open class Viewing : ScreenAdapter() {
    // convenience class also containing whether focus is on a modal
    class ModalWithFocus(var modal : Modal, var focus : Boolean)

    // batches
    private var modalBatch = SpriteBatch()
    private var popupBatch = SpriteBatch()
    private var overlayBatch = SpriteBatch()

    // stages
    private var modalStage = Stage()
    private var popupStage = Stage()
    private var overlayStage = Stage()

    // cameras
    private var modalCamera = OrthographicCamera().apply {
        setToOrtho(false, Constants.SCREEN_WIDTH.toFloat(), Constants.SCREEN_HEIGHT.toFloat())
    }
    private var popupCamera = OrthographicCamera().apply {
        setToOrtho(false, Constants.SCREEN_WIDTH.toFloat(), Constants.SCREEN_HEIGHT.toFloat())
    }
    private var overlayCamera = OrthographicCamera().apply {
        setToOrtho(false, Constants.SCREEN_WIDTH.toFloat(), Constants.SCREEN_HEIGHT.toFloat())
    }

    // modals take control of input
    var modals = mutableListOf<ModalWithFocus>()

    // popups take control of input above modals
    var popups = mutableListOf<ModalWithFocus>()

    // overlays do not take control of input but can add their own input checks
    var overlays = mutableListOf<ModalWithFocus>()

    // add a modal to list for potential displaying
    fun addModal(modal : Modal) {
        modals.add(ModalWithFocus(modal, false));
        modalStage.addActor(modal)
    }

    // set something to display
    fun show(modal : Modal) {
        showModal(modal)
        showPopup(modal)
        showOverlay(modal)
    }

    // hide something
    fun hide(modal : Modal) {
        hideModal(modal)
        hidePopup(modal)
        hideOverlay(modal)
    }

    // remove something
    fun remove(modal : Modal) {
        removeModal(modal)
        removePopup(modal)
        removeOverlay(modal)
    }

    // set something in focus
    fun setFocus(modal : Modal, focusState : Boolean) {
        setModalFocus(modal, focusState)
        setPopupFocus(modal, focusState)
        setOverlayFocus(modal, focusState)
    }

    // set that modal to display
    fun showModal(modal : Modal) {
        for(modalWithFocus in modals) {
            if(modalWithFocus.modal === modal) {
                modalWithFocus.modal.display = true
            }
        }
    }

    // hide a modal
    fun hideModal(modal : Modal) {
        for(modalWithFocus in modals) {
            if(modalWithFocus.modal === modal) {
                modalWithFocus.modal.display = false
            }
        }
    }

    // remove modal from this viewing
    fun removeModal(modal : Modal) : Boolean {
        for(modalWithFocus in modals) {
            if(modalWithFocus.modal === modal) {
                modalWithFocus.modal.remove()
                return modals.remove(modalWithFocus)
            }
        }
        return false
    }

    // set a modal in focus
    fun setModalFocus(modal : Modal, focusState : Boolean) {
        // only one can be set true at a time
        // (if setting to unfocused, just change the one)
        if(focusState) {
            for(modalWithFocus in modals) {
                if(modalWithFocus.modal === modal) {
                    modalWithFocus.focus = true
                    modalWithFocus.modal.allowKeyboardControl = true

                    // last item is draw is drawn over all else
                    modals.remove(modalWithFocus)
                    modals.add(modalWithFocus)
                } else {
                    modalWithFocus.focus = false
                    modalWithFocus.modal.allowKeyboardControl = false
                }
            }
        } else {
            for(modalWithFocus in modals) {
                if(modalWithFocus.modal === modal) {
                    modalWithFocus.focus = false
                    modalWithFocus.modal.allowKeyboardControl = false
                }
            }
        }
    }

    // add popup to view for potential displaying
    fun addPopup(popup : Modal) {
        popups.add(ModalWithFocus(popup, false));
        popupStage.addActor(popup)
    }

    // display popup
    fun showPopup(popup : Modal) {
        for(modalWithFocus in popups) {
            if(modalWithFocus.modal === popup) {
                modalWithFocus.modal.display = true
            }
        }
    }

    // hide popup
    fun hidePopup(popup : Modal) {
        for(modalWithFocus in popups) {
            if(modalWithFocus.modal === popup) {
                modalWithFocus.modal.display = false
            }
        }
    }

    // remove popup from viewing
    fun removePopup(popup : Modal) : Boolean {
        for(modalWithFocus in popups) {
            if(modalWithFocus.modal === popup) {
                modalWithFocus.modal.remove()
                return popups.remove(modalWithFocus)
            }
        }
        return false
    }

    // set focus to popup
    fun setPopupFocus(popup : Modal, focusState : Boolean) {
        // only one can be set true at a time
        // (if setting to unfocused, just change the one)
        if(focusState) {
            for(popupWithFocus in popups) {
                if(popupWithFocus.modal === popup) {
                    popupWithFocus.focus = true
                    popupWithFocus.modal.allowKeyboardControl = true

                    popups.remove(popupWithFocus)
                    popups.add(popupWithFocus)
                } else {
                    popupWithFocus.focus = false
                    popupWithFocus.modal.allowKeyboardControl = false
                }
            }
        } else {
            for(popupWithFocus in popups) {
                if(popupWithFocus.modal === popup) {
                    popupWithFocus.focus = false
                    popupWithFocus.modal.allowKeyboardControl = false
                }
            }
        }
    }

    // add an overlay to the viewing (for potential display)
    fun addOverlay(overlay : Modal) {
        overlays.add(ModalWithFocus(overlay, false))
        overlayStage.addActor(overlay)
    }


    // display an overlay
    fun showOverlay(overlay : Modal) {
        for(overlayWithFocus in overlays) {
            if(overlayWithFocus.modal === overlay) {
                overlayWithFocus.modal.display = true
            }
        }
    }

    // hide an overlay
    fun hideOverlay(overlay : Modal) {
        for(overlayWithFocus in overlays) {
            if(overlayWithFocus.modal === overlay) {
                overlayWithFocus.modal.display = false
            }
        }
    }

    // remove overlay from viewing
    fun removeOverlay(overlay : Modal) : Boolean {
        for(overlayWithFocus in overlays) {
            if(overlayWithFocus.modal === overlay) {
                overlayWithFocus.modal.remove()
                return overlays.remove(overlayWithFocus)
            }
        }
        return false
    }

    // just moves an overlay above the rest if set true
    fun setOverlayFocus(overlay : Modal, focusState : Boolean) {
        if(focusState) {
            for(modalWithFocus in modals) {
                if(modalWithFocus.modal === overlay) {
                    modalWithFocus.focus = true

                    // last item is draw is drawn over all else
                    modals.remove(modalWithFocus)
                    modals.add(modalWithFocus)
                } else {
                    modalWithFocus.focus = false
                }
            }
        } else {
            for(modalWithFocus in modals) {
                if(modalWithFocus.modal === overlay) {
                    modalWithFocus.focus = false
                }
            }
        }
    }

    // check if any modals or popups are active (if true, all keyboard access in this viewing disabled so keyboard
    // takes over in active modals/popups)
    private fun isAnythingActive() : Boolean {
        var modalActive = false
        var popupActive = false

        for(modalWithFocus in modals) {
            if(modalWithFocus.focus || modalWithFocus.modal.display) {
                modalActive = true
            }
        }
        for(modalWithFocus in popups) {
            if(modalWithFocus.focus || modalWithFocus.modal.display) {
                popupActive = true
            }
        }

        return modalActive || popupActive
    }

    // if any popups/modals, then keyboard actions in overlays rendered inactive
    // todo there may be situations where a global keycheck is required, eg a button to save game or something
    private var lastKBContorlWrite = false;
    private fun setOverlayKBControl(state : Boolean) {
        if(lastKBContorlWrite != state) {
            for(modalWithFocus in overlays) {
                modalWithFocus.modal.allowKeyboardControl = state
            }
            lastKBContorlWrite = state
        }
    }

    override fun render(delta: Float) {
        super.render(delta)

        // check if we are allowed to handle keyboard input
        if(!isAnythingActive()) {
            handleKeys()
            setOverlayKBControl(true)
        } else {
            setOverlayKBControl(false)
        }

        // draw overlays first
        overlayCamera.update()
        overlayBatch.use {
            overlayStage.act(delta)
            overlayStage.draw()
        }

        // draw modals second
        modalCamera.update()
        modalBatch.use {
            modalStage.act(delta)
            modalStage.draw()
        }

        // draw popus third (and foremost)
        popupCamera.update()
        popupBatch.use {
            popupStage.act(delta)
            popupStage.draw()
        }
    }

    // handles keys
    open fun handleKeys() {

    }

    override fun dispose() {
        super.dispose()

        overlayStage.dispose()
        modalStage.dispose()
        popupStage.dispose()

        overlayBatch.dispose()
        modalBatch.dispose()
        popupBatch.dispose()
    }
}