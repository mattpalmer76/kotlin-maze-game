package com.mygdx.game.classes

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.mygdx.game.helper.Assets

open class Modal(title : String, skin : Skin) : Window(title, skin) {
    open var modalWidth = 600f
    open var modalHeight = 520f

    // allow keyboard control
    var allowKeyboardControl = true

    // as this is a modal, default to no display
    open var display = false;

    // do we always reconstruct table?
    open var alwaysRefreshTable = false;
    lateinit var table : Table

    init {
        // create the table to be displayed in window
        refreshTable();
    }

    override fun draw(batch : Batch, parentAlpha : Float) {
        // if displaying...
        if(display) {
            // check keys
            if(allowKeyboardControl)
                handleKeys()

            // and if continually refreshing...
            if(alwaysRefreshTable)
                refreshTable()

            // draw
            super.draw(batch, parentAlpha)
        }
    }

    // reshesh the table
    open fun refreshTable() {
        createTable()

        this.clear()
        this.add(table)
        this.setSize(modalWidth, modalHeight)
        this.style = Assets.skin.get("dialog", Window.WindowStyle::class.java)
        this.style = Assets.skin.get("dialog", Window.WindowStyle::class.java)
    }

    // create the table
    open fun createTable() {
        table = Table()
    }

    // for handling key presses
    open fun handleKeys() {
        // do nothing
    }
}