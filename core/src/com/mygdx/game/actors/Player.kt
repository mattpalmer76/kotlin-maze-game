package com.mygdx.game.actors

import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.map_element_attributes.BlockAttributes
import com.mygdx.game.helper.map_element_attributes.ClimbableAttributes
import com.mygdx.game.helper.map_element_attributes.ItemAttributes
import com.mygdx.game.model.GameLevel
import com.mygdx.game.model.Location
import kotlin.math.floor

class Player(location : Location) : Being(Assets.playerMTAs, location) {
    var holding = ItemAttributes.Companion.ItemAttribute.EMPTY

    var lives = 3
    private var fallAmount = 0

    enum class StabilityState {
        STABLE,
        FALLING
    }
    var stabilityState = StabilityState.STABLE

    fun moveLeft(level : GameLevel) : Boolean {
        var canMove = false

        if(xOnMap != null && yOnMap != null) {
            var tileX = tileX(xOnMap!!)
            var tileY = tileY(yOnMap!!)
            var xInTile = xOnMap!! - (tileX * Constants.SCALED_TILE_WIDTH)
            var yInTile = yOnMap!! - (tileY * Constants.SCALED_TILE_HEIGHT)

            if(yOnMap!! > tileY * Constants.SCALED_TILE_HEIGHT) {
                if(level.isEmpty(tileX - 1, tileY)) {
                    // can dismount a climbable if just a little above the 'walk line'
                    if(level.canWalkOn(tileX - 1, tileY - 1)) {
                        if(yInTile < Constants.SCALING * 5) {
                            yOnMap = (tileY * Constants.SCALED_TILE_HEIGHT)
                        }
                    }
                    // todo do fall
                } else if (level.isEmpty(tileX - 1,tileY + 1) && yInTile > (Constants.SCALED_TILE_HEIGHT - Constants.SCALING * 5)) {
                    // can dismount if just a little below the walk line (and there is a blocking tile to the left)
                    yOnMap = ((tileY + 1) * Constants.SCALED_TILE_HEIGHT)
                }
            } else
            // if there's a block underneath
            if(level.canWalkOn(tileX, tileY - 1) || level.isClimbable(tileX, tileY)) {
                if((xOnMap!! - Constants.SCALING)/Constants.SCALED_TILE_WIDTH < tileX) {
                    if(
                            level.canWalkOn(tileX - 1, tileY - 1) &&
                            level.isPassable(tileX - 1, tileY) ||
                            level.isClimbable(tileX - 1, tileY)
                            ) {
                        // if there's a stable floor beneath, allow movement
                        canMove = true
                    } else if(
                            level.isEmpty(tileX - 1, tileY - 1) &&
                            level.isEmpty(tileX - 1, tileY)
                    ) {
                        // check if we have a plank
                        if(holding == ItemAttributes.Companion.ItemAttribute.PLANK) {
                            BlockAttributes.setPlank(tileX-1, tileY-1, level)
                            holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                            canMove = true
                        }
                    } else if(
                            // if is a door and we have a key...
                            level.isDoor(tileX + 1, tileY) &&
                            holding == ItemAttributes.Companion.ItemAttribute.KEY) {
                        level.clearBlock(tileX + 1, tileY)
                        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                        canMove = true
                    }
                } else {
                    if(level.canWalkOn(tileX, tileY-1) && level.isPassable(tileX, tileY))
                        // if in middle of empty tile with solid floor beneath, allow movement
                        if(xInTile == 0 && level.isPassable(tileX - 1, tileY))
                            canMove = true
                        else if(xInTile > 0) {
                            canMove = true
                        }

                }
            }

            if(level.isArrowRight(tileX, tileY)) {
                canMove = false
            }

            if(canMove) {
                xOnMap = xOnMap?.minus(Constants.SCALING)
                movementType = Being.MovementType.LEFT
            } else {
                if(xInTile == 0) {
                    if(level.isDoor(tileX-1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.KEY) {
                        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                        level.clearBlock(tileX-1, tileY)
                    } else if(level.isEmptyHoop(tileX-1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.ROPE) {
                        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                        level.hangRope(tileX-1, tileY)
                    } else if(level.isWater(tileX-1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.EMPTY_BUCKET) {
                        holding = ItemAttributes.Companion.ItemAttribute.FULL_BUCKET
                    } else if(level.isMud(tileX-1, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.BOOTS) {
                        xOnMap = xOnMap?.minus(Constants.SCALING)
                        movementType = Being.MovementType.LEFT
                        yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                    } else if(level.isWater(tileX-1, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.LIFE_RING) {
                        xOnMap = xOnMap?.minus(Constants.SCALING)
                        movementType = Being.MovementType.LEFT
                        yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                    }
                }
                if(level.isMud(tileX, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.BOOTS) {
                    xOnMap = xOnMap?.minus(Constants.SCALING)
                    movementType = Being.MovementType.LEFT
                    yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                } else if(level.isWater(tileX, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.LIFE_RING) {
                    xOnMap = xOnMap?.minus(Constants.SCALING)
                    movementType = Being.MovementType.LEFT
                    yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                } else {
                    movementType = Being.MovementType.IDLE
                }
            }
        }

        return canMove
    }

    // Logic for what happens when moving in a direction

    fun moveRight(level : GameLevel) : Boolean {
        var canMove = false

        if(xOnMap != null && yOnMap != null) {
            var tileX = tileX(xOnMap!!)
            var tileY = tileY(yOnMap!!)
            var xInTile = xOnMap!! - (tileX * Constants.SCALED_TILE_WIDTH)
            var yInTile = yOnMap!! - (tileY * Constants.SCALED_TILE_HEIGHT)

            if(yOnMap!! > tileY * Constants.SCALED_TILE_HEIGHT) {
                if(level.isEmpty(tileX + 1, tileY)) {
                    // can dismount a climbable if just a little above the 'walk line'
                    if(level.canWalkOn(tileX + 1, tileY - 1)) {
                        if(yInTile < Constants.SCALING * 5) {
                            yOnMap = (tileY * Constants.SCALED_TILE_HEIGHT)
                        }
                    }
                    // todo do fall
                } else if (level.isEmpty(tileX + 1,tileY + 1) && yInTile > (Constants.SCALED_TILE_HEIGHT - Constants.SCALING * 5)) {
                    // can dismount if just a little below the walk line (and there is a blocking tile to the left)
                    yOnMap = ((tileY + 1) * Constants.SCALED_TILE_HEIGHT)
                }
            } else
            // if there's a block underneath
            if(level.canWalkOn(tileX, tileY - 1) || level.isClimbable(tileX, tileY)) {
                var test = level.isClimbable(tileX, tileY)
                if((xOnMap!! - Constants.SCALING)/Constants.SCALED_TILE_WIDTH < tileX) {
                    if(
                            (level.canWalkOn(tileX + 1, tileY - 1) &&
                            level.isPassable(tileX + 1, tileY) &&
                            level.canWalkOn(tileX + 1, tileY - 1) ) ||
                             level.isClimbable(tileX + 1, tileY)
                    ) {
                        // if there's a stable floor beneath, allow movement
                        canMove = true
                    } else if(
                            level.isEmpty(tileX + 1, tileY - 1) &&
                            level.isEmpty(tileX + 1, tileY)
                    ) {
                        // check if we have a plank
                        if(holding == ItemAttributes.Companion.ItemAttribute.PLANK) {
                            BlockAttributes.setPlank(tileX+1, tileY-1, level)
                            holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                            canMove = true
                        }
                    } else if(
                        // if is a door and we have a key...
                            level.isDoor(tileX + 1, tileY) &&
                        holding == ItemAttributes.Companion.ItemAttribute.KEY) {
                            level.clearBlock(tileX + 1, tileY)
                            holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                            canMove = true
                        }
                } else if(
                        // if just about to enter the next tile
                        (level.isClimbable(tileX + 1, tileY) && level.isPassable(tileX + 1, tileY) &&
                        xInTile >= Constants.SCALED_TILE_WIDTH) ||
                        (level.canWalkOn(tileX, tileY-1) && level.isPassable(tileX, tileY)
                                && level.isPassable(tileX+1, tileY) && level.canWalkOn(tileX+1, tileY-1))) {
                            canMove = true
                        }
                } else {
                    // if in middle of empty tile with solid floor beneath, allow movement
                        var test1 = level.canWalkOn(tileX, tileY-1)
                        var test2 = level.isPassable(tileX, tileY)
                    if(level.canWalkOn(tileX, tileY+1) && level.isPassable(tileX, tileY))
                        canMove = true
                }

            if(level.isArrowLeft(tileX+1, tileY)) {
                canMove = false
            }

            if(canMove) {
                xOnMap = xOnMap?.plus(Constants.SCALING)
                movementType = Being.MovementType.RIGHT
            } else {
                if(xInTile == 0) {
                    if(level.isDoor(tileX+1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.KEY) {
                        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                        level.clearBlock(tileX+1, tileY)
                    } else if(level.isEmptyHoop(tileX+1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.ROPE) {
                        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
                        level.hangRope(tileX+1, tileY)
                    } else if(level.isWater(tileX+1, tileY) && holding == ItemAttributes.Companion.ItemAttribute.EMPTY_BUCKET) {
                        holding = ItemAttributes.Companion.ItemAttribute.FULL_BUCKET
                    } else if(level.isMud(tileX+1, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.BOOTS) {
                        xOnMap = xOnMap?.plus(Constants.SCALING)
                        movementType = Being.MovementType.RIGHT
                        yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                    } else if(level.isWater(tileX+1, tileY) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.LIFE_RING) {
                        xOnMap = xOnMap?.plus(Constants.SCALING)
                        movementType = Being.MovementType.RIGHT
                        yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                    }
                }
                if((level.isMud(tileX + 1, tileY) || level.isMud(tileX, tileY)) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.BOOTS) {
                    xOnMap = xOnMap?.plus(Constants.SCALING)
                    movementType = Being.MovementType.RIGHT
                    yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                } else if((level.isWater(tileX + 1, tileY) || level.isWater(tileX, tileY)) && yInTile < Constants.SCALING * 5 && holding == ItemAttributes.Companion.ItemAttribute.LIFE_RING) {
                    xOnMap = xOnMap?.plus(Constants.SCALING)
                    movementType = Being.MovementType.RIGHT
                    yOnMap = tileY * Constants.SCALED_TILE_HEIGHT
                } else {
                    movementType = Being.MovementType.IDLE
                }
            }
        }

        return canMove
    }

    fun moveUp(level : GameLevel) : Boolean {
        var canMove = false

        var x = 0
        var y = 0
        var adjustedX = 0
        if(xOnMap != null && yOnMap != null) {
            x = tileX(xOnMap!!)
            y = tileY(yOnMap!!)
            var xInBlock = xOnMap!! - (x * Constants.SCALED_TILE_WIDTH)

            if(xInBlock >= 0 && xInBlock < Constants.SCALED_TILE_HEIGHT * 2 ||
                    xInBlock < Constants.SCALED_TILE_WIDTH && xInBlock >= Constants.SCALED_TILE_WIDTH - Constants.SCALED_TILE_WIDTH * 2) {
                adjustedX = tileX(xOnMap!! + Constants.SCALING * 2)
                if(ClimbableAttributes.isClimbable(adjustedX, y,level)) {
                    if (yOnMap!! > y * Constants.SCALED_TILE_HEIGHT) {
                        canMove = true
                    } else if (ClimbableAttributes.isClimbable(adjustedX, y + 1, level)) {
                        canMove = true
                    }
                }
            }
        }

        if(canMove) {
            // center on climbable
            xOnMap = adjustedX * Constants.SCALED_TILE_WIDTH

            yOnMap = yOnMap?.plus(Constants.SCALING)
            movementType = Being.MovementType.UP
        } else {
            movementType = Being.MovementType.IDLE
        }

        return canMove
    }

    fun moveDown(level : GameLevel) : Boolean {
        var canMove = false

        var x = 0
        var y = 0
        var adjustedX = 0
        if(xOnMap != null && yOnMap != null) {
            x = tileX(xOnMap!!)
            y = tileY(yOnMap!!)
            var xInBlock = xOnMap!! - (x * Constants.SCALED_TILE_WIDTH)

            if(xInBlock >= 0 && xInBlock < Constants.SCALED_TILE_HEIGHT * 2 ||
                xInBlock < Constants.SCALED_TILE_WIDTH && xInBlock >= Constants.SCALED_TILE_WIDTH - Constants.SCALED_TILE_WIDTH * 2) {
                adjustedX = tileX(xOnMap!! + Constants.SCALING * 2)
                if(ClimbableAttributes.isClimbable(adjustedX, y,level)) {
                    if (yOnMap!! > y * Constants.SCALED_TILE_HEIGHT) {
                        canMove = true
                    } else if (ClimbableAttributes.isClimbable(adjustedX, y - 1, level)) {
                        canMove = true
                    }
                }
            }
        }

        if(canMove) {
            // center on climbable
            xOnMap = adjustedX * Constants.SCALED_TILE_WIDTH

            yOnMap = yOnMap?.minus(Constants.SCALING)
            movementType = Being.MovementType.DOWN
        } else {
            movementType = Being.MovementType.IDLE
        }

        return canMove
    }

    private fun tileX(x : Int): Int {
        return floor(x.toFloat() / Constants.SCALED_TILE_WIDTH.toFloat()).toInt()
    }

    private fun tileY(y : Int): Int {
        return floor(y.toFloat() / Constants.SCALED_TILE_HEIGHT.toFloat()).toInt()
    }

    fun getOnscreenX(x : Int) : Int {
        if(xOnMap != null)
            return xOnMap!! - x
        return 0
    }

    fun getOnscreenY(y : Int) : Int {
        if(yOnMap != null)
            return yOnMap!! - y
        return 0
    }

    fun setUpSpritePos(x : Int, y : Int) {
        spritePos(getOnscreenX(x), getOnscreenY(y))
    }

    fun beIdle() {
        movementType = Being.MovementType.IDLE
    }

    fun pickupOrDrop(level : GameLevel) {

        var x = tileX(xOnMap!! + (Constants.SCALED_TILE_WIDTH / 2))
        var y = tileY(yOnMap!!)

        var item = level.getItemXY(x, y)

        if(!BlockAttributes.isBlocking(x, y, level) && BlockAttributes.isBlocking(x, y-1, level)) {
            if(holding == ItemAttributes.Companion.ItemAttribute.FULL_BUCKET && level.isSeed(x, y)) {
                level.growVine(x, y)
                holding = ItemAttributes.Companion.ItemAttribute.EMPTY
            } else if(holding != ItemAttributes.Companion.ItemAttribute.EMPTY && item != ItemAttributes.getItemNumber(ItemAttributes.Companion.ItemAttribute.EMPTY)) {
                exchange(level, x, y, item)
            } else if(holding != ItemAttributes.Companion.ItemAttribute.EMPTY && item == ItemAttributes.getItemNumber(ItemAttributes.Companion.ItemAttribute.EMPTY)) {
                drop(level, x, y, item)
            } else {
                pickup(level,x ,y, item)
            }
        }
    }

    private fun exchange(level : GameLevel, x: Int, y: Int, item : Int) {
        var exchange = ItemAttributes.getItemNumber(holding)
        holding = ItemAttributes.getItemEnum(item)!!
        level.setItemXY(x, y, exchange)
    }

    private fun pickup(level : GameLevel, x : Int, y : Int, item : Int) {
        holding = ItemAttributes.getItemEnum(item)!!
        level.setItemXY(x, y, ItemAttributes.getItemNumber(ItemAttributes.Companion.ItemAttribute.EMPTY))
    }

    private fun drop(level : GameLevel, x : Int, y : Int, item : Int) {
        level.setItemXY(x, y, ItemAttributes.getItemNumber(holding))
        holding = ItemAttributes.Companion.ItemAttribute.EMPTY
    }


}