package com.mygdx.game.actors

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.Actor
import com.mygdx.game.config.Constants
import com.mygdx.game.model.Location
import com.mygdx.game.model.MovementTextureAtlantes
import kotlin.math.floor

open class Being(var movementTextureAtlantes: MovementTextureAtlantes, var location : Location) : Actor() {

    // five movements - possibly six later
    public enum class MovementType {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        IDLE
    }

    companion object {
        private var lastFrame = ""
        private var lastRandomFrame = "0"
    }

    public var xOnMap : Int? = null
    public var yOnMap : Int? = null

    // initialize as idle
    var movementType : MovementType = MovementType.IDLE
    set(value) {
        if(field != value) {
            frame = 0
            field = value
        }
    }

    // setting first frame as idle "0"
    private var sprite : Sprite = Sprite(movementTextureAtlantes.idle.findRegion("0"))

    // the max frame for select movement
    private var maxFrame = movementTextureAtlantes.framesIdle * movementTextureAtlantes.slowdownMultiplier

    // the frame (incl. slowdown multiplier)
    private var frame : Int = 0
    private set(value) {
        field = if(value >= maxFrame) {
            value - maxFrame
        } else {
            value
        }
    }

    // where the selected sprite will be drawn onscreen
    private var xSpritePos = 0f
    private var ySpritePos = 0f

    init {
        // as first sprite assigned above, might as well set size here
        sprite.setSize(Constants.SCALED_TILE_HEIGHT.toFloat(), Constants.SCALED_TILE_WIDTH.toFloat())

        // use location to get XY for drawing onscreen
        setXYOnMap()
    }

    private fun setXYOnMap() {
        if(location.x != null) {
            xOnMap = location.x!! * Constants.SCALED_TILE_WIDTH
        }
        if(location.y != null) {
            yOnMap = location.y!! * Constants.SCALED_TILE_HEIGHT
        }
    }

    override fun act(delta : Float) {
        super.act(delta)
    }

    // allow for setting sprite position onscreen.
    public fun spritePos(x : Float, y : Float) {
        xSpritePos = x
        ySpritePos = y
    }

    // same but with integers
    public fun spritePos(x : Int, y : Int) {
        spritePos(x.toFloat(), y.toFloat())
    }

    // draw based on on state
    override fun draw(batch : Batch, delta : Float) {
        // get sprite based on state and on frame
        var frameStringForIdle = if(movementTextureAtlantes.randomiseIdle == true) {
            randomFrame(movementTextureAtlantes.slowdownMultiplierIdle)
        } else {
            trueFrame(movementTextureAtlantes.slowdownMultiplierIdle)
        }
        when(movementType) {
            MovementType.UP -> {
                maxFrame = movementTextureAtlantes.framesUp * movementTextureAtlantes.slowdownMultiplier
                sprite= Sprite(movementTextureAtlantes.up.findRegion(trueFrame(movementTextureAtlantes.slowdownMultiplier)))
            }
            MovementType.DOWN -> {
                maxFrame = movementTextureAtlantes.framesDown * movementTextureAtlantes.slowdownMultiplier
                sprite= Sprite(movementTextureAtlantes.down.findRegion(trueFrame(movementTextureAtlantes.slowdownMultiplier)))
            }
            MovementType.LEFT -> {
                maxFrame = movementTextureAtlantes.framesLeft * movementTextureAtlantes.slowdownMultiplier
                sprite= Sprite(movementTextureAtlantes.left.findRegion(trueFrame(movementTextureAtlantes.slowdownMultiplier)))
            }
            MovementType.RIGHT -> {
                maxFrame = movementTextureAtlantes.framesRight * movementTextureAtlantes.slowdownMultiplier
                sprite= Sprite(movementTextureAtlantes.right.findRegion(trueFrame(movementTextureAtlantes.slowdownMultiplier)))
            }
            MovementType.IDLE -> {
                maxFrame = movementTextureAtlantes.framesIdle * movementTextureAtlantes.slowdownMultiplierIdle
                sprite= Sprite(movementTextureAtlantes.idle.findRegion(frameStringForIdle))
            }
        }

        // set scaled size
        sprite.setSize(Constants.SCALED_TILE_HEIGHT.toFloat(), Constants.SCALED_TILE_WIDTH.toFloat())

        // set position and bouts
        sprite.setPosition(xSpritePos, ySpritePos)

        // increment frame
        frame++

        // draw sprite
        sprite.draw(batch)
    }

    // gets the 'true frame', that is the frame divided by the slowdown multiplier (floor)
    private fun trueFrame(slowdownMultiplier : Int) : String {
        return floor(frame.toDouble() / slowdownMultiplier.toDouble()).toInt().toString()
    }

    private fun randomFrame(slowDownMultiplier: Int) : String {
        if(lastFrame != trueFrame(slowDownMultiplier)) {
            lastFrame = trueFrame(slowDownMultiplier)
            lastRandomFrame = (Math.random() * (movementTextureAtlantes.framesIdle - 1)).toInt().toString()
        }
        return lastRandomFrame
    }
}