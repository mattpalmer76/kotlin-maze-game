package com.mygdx.game.actors

import com.mygdx.game.model.Location
import com.mygdx.game.model.MovementTextureAtlantes

class Enemy(movementTextureAtlantes: MovementTextureAtlantes, location : Location, var smartAI : Boolean = false) : Being(movementTextureAtlantes, location) {
    // do some kind of AI here
}