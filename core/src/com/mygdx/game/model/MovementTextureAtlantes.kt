package com.mygdx.game.model

import com.badlogic.gdx.graphics.g2d.TextureAtlas

class MovementTextureAtlantes(
        var idle : TextureAtlas,
        var up : TextureAtlas,
        var down : TextureAtlas,
        var left : TextureAtlas,
        var right : TextureAtlas,
        var slowdownMultiplier : Int = 4,
        var framesIdle : Int = 8,
        var framesUp : Int = 8,
        var framesDown : Int = 8,
        var framesLeft : Int = 8,
        var framesRight : Int = 8,
        var slowdownMultiplierIdle : Int = 4,
        var randomiseIdle : Boolean = false
        )