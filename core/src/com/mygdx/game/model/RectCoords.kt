package com.mygdx.game.model

import org.json.JSONObject

class RectCoords(var x1 : Int, var y1 : Int, var x2 : Int, var y2 : Int) {
    companion object {
        const val X1_KEY = "x1"
        const val Y1_KEY = "y1"
        const val X2_KEY = "x2"
        const val Y2_KEY = "y2"
    }

    // check an x/y coord is within bounds
    fun isWithinBounds(x : Int, y : Int) : Boolean {
        return x in x1..x2 && y in y1..y2
    }

    constructor(jo : JSONObject) : this(0, 0, 0, 0) {
        if(jo.has(X1_KEY)) {
            x1 = jo.getInt(X1_KEY)
        }
        if(jo.has(Y1_KEY)) {
            y1 = jo.getInt(Y1_KEY)
        }
        if(jo.has(X2_KEY)) {
            x2 = jo.getInt(X2_KEY)
        }
        if(jo.has(Y2_KEY)) {
            y2 = jo.getInt(Y2_KEY)
        }
        fixNumericalOrder()
    }

    init {
        // todo not sure if this is called after custom constructor or not!
        fixNumericalOrder()
    }

    public fun keepVarsInLevelArea() : RectCoords {
        if(x1 < 0)
            x1 = 0
        if(x1 >= Level.X_SIZE)
            x1 = Level.X_SIZE - 1
        if(y1 < 0)
            y1 = 0
        if(y1 >= Level.Y_SIZE)
            y1 = Level.Y_SIZE -1
        if(x2 < 0)
            x2 = 0
        if(x2 >= Level.X_SIZE)
            x2 = Level.X_SIZE - 1
        if(y2 < 0)
            y2 = 0
        if(y2 >= Level.Y_SIZE)
            y2 = Level.Y_SIZE -1
    
        return this
    }

    private fun fixNumericalOrder() {
        if(x2 < x1) {
            val temp = x1
            x1 = x2
            x2 = temp
        }
        if(y2 < y1) {
            val temp = y1
            y1 = y2
            y2 = temp
        }
    }

    public fun getJSONObject() : JSONObject {
        var jo = JSONObject()

        jo.put(X1_KEY, x1)
        jo.put(Y1_KEY, y1)
        jo.put(X2_KEY, x2)
        jo.put(Y2_KEY, y2)

        return jo
    }
}