package com.mygdx.game.model

import com.mygdx.game.helper.map_element_attributes.BlockAttributes
import com.mygdx.game.helper.map_element_attributes.ClimbableAttributes
import com.mygdx.game.helper.map_element_attributes.ObstacleAttributes
import org.json.JSONObject

// todo work in progress
// This is the same as Level class but also contains information on enemy positions while playing the game
// and any other additional information needed.
// Will likely also likely handle info on
// * allowed movement by player,
// * what happens to the player when they move a specific direction (fall, stop, ascend staircase),
// * picking up and dropping of items,
// * the use of items on map and enemies,
// * interaction with enemies,
// * and completion of a level.
// * Also likely to include AI for enemies here too - simple at first but maybe more ambitious later on

class GameLevel : Level {
    constructor() : super() {
        setupEnemyStarts()
    }

    constructor(jo : JSONObject) : super(jo) {
        // todo load mostly ai stuff most likely
    }

    // todo setup enemy positions of game init
    fun setupEnemyStarts() {

    }

    fun clearBlock(tileX : Int, tileY : Int) {
        setBlockXY(tileX, tileY, 0)
    }

    // helper functions to determine movement stuff
    fun isBlocking(x : Int, y : Int) : Boolean {
        return BlockAttributes.isBlocking(x, y, this) && !ClimbableAttributes.isClimbable(x, y, this) && isObstacle(x, y)
    }

    fun isEmpty(x : Int, y : Int) : Boolean {
        return !BlockAttributes.isBlocking(x, y, this) &&
                !ClimbableAttributes.isClimbable(x, y, this) &&
                !BlockAttributes.isDoor(x, y, this) &&
                !isObstacle(x, y)
    }

    fun canWalkOn(x : Int, y : Int) : Boolean {
        return BlockAttributes.isBlocking(x, y, this) || ClimbableAttributes.isClimbable(x, y, this) || BlockAttributes.isDoor(x, y, this)
    }

    fun isPassable(x : Int, y : Int) : Boolean {
        return !BlockAttributes.isBlocking(x, y, this) &&
                !BlockAttributes.isDoor(x, y, this) &&
                !ClimbableAttributes.isEmptyHoop(x, y, this) &&
                !isObstacle(x, y)
    }

    fun isSeed(x : Int, y : Int) : Boolean {
        return BlockAttributes.isSeed(x, y, this)
    }

    fun isArrowLeft(x : Int, y : Int) : Boolean {
        return BlockAttributes.isLeftOnly(x, y, this)
    }

    fun isArrowRight(x : Int, y : Int) : Boolean {
        return BlockAttributes.isRightOnly(x, y, this)
    }

    fun isDoor(x : Int, y : Int) : Boolean {
        return BlockAttributes.isDoor(x, y, this)
    }

    fun isClimbable(x : Int, y : Int) : Boolean {
        return ClimbableAttributes.isClimbable(x, y, this)
    }

    fun isEmptyHoop(x : Int, y : Int) : Boolean {
        return ClimbableAttributes.isEmptyHoop(x, y, this)
    }

    fun isObstacle(x : Int, y : Int) : Boolean {
        return ObstacleAttributes.isObstacle(x, y, this)
    }

    fun isWater(x : Int, y : Int) : Boolean {
        return ObstacleAttributes.isObstacle(x, y, this)
    }

    fun isMud(x : Int, y : Int) : Boolean {
        return ObstacleAttributes.isMud(x, y, this)
    }

    fun hangRope(tileX : Int, tileY : Int) {
        var lastEmpty = 0

        // find where the rope hang will end (last available is always 0)
        for(findY in tileY - 1 downTo 0) {
            if(!isEmpty(tileX, findY-1)) {
                lastEmpty = findY
                break
            }
        }

        ClimbableAttributes.setToClimbableHoop(tileX, tileY, this)
        ClimbableAttributes.setToRopeEnd(tileX, lastEmpty, this)

        for(drawY in tileY - 1 downTo lastEmpty + 1) {
            ClimbableAttributes.setToRope(tileX, drawY, this)
        }
    }

    fun growVine(tileX : Int, tileY : Int) {
        var lastEmpty = Level.Y_SIZE - 1

        BlockAttributes.clearBlock(tileX, tileY, this)
        ClimbableAttributes.setVineStart(tileX, tileY, this)

        for(findY in tileY + 1 until Level.Y_SIZE) {
            if(!isEmpty(tileX, findY)) {
                lastEmpty = findY - 1
                break
            }
        }

        for(drawY in tileY + 1 .. lastEmpty) {
            ClimbableAttributes.setVine(tileX, drawY, this)
        }
    }

    override fun initialiseProperties() {
        super.initialiseProperties()
    }

    override fun getJSONObject(): JSONObject {
        var jo = super.getJSONObject()

        // todo add additional data here

        return jo;
    }
}