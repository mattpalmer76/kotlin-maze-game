package com.mygdx.game.model

import com.mygdx.game.config.Constants
import org.json.JSONObject

class EnemyLocation(var enemyIndex : Int? = null, x : Int? = null, y : Int? = null) : Location(x, y) {
    companion object {
        val ENEMY_INDEX_KEY = "enemy_index"
    }

    public fun isEnemyActive() : Boolean {
        return x != null && y != null && enemyIndex != null;
    }

    public fun isEnemyInBounds() : Boolean {
        if(isEnemyActive()) {
            if(x!! >= 0 && y!! >= 0 && x!! < Level.X_SIZE && y!! < Level.Y_SIZE) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    // constructor to accept json object - for loading data from disk
    constructor(jo : JSONObject) : this() {
        // there has to be a better way to do this
        // todo find out how to use super constructor
        enemyIndex = if(jo.has(ENEMY_INDEX_KEY)) {
            jo.getInt(ENEMY_INDEX_KEY)
        } else {
            null
        }
        enemyIndex = if(enemyIndex == -1) {
            null
        } else {
            enemyIndex
        }

        x = if(jo.has(X_KEY)) {
            jo.getInt(X_KEY)
        } else {
            null;
        }
        x = if(x == -1) {
            null
        } else {
            x
        }

        y = if(jo.has(Y_KEY)) {
            jo.getInt(Y_KEY)
        } else {
            null
        }
        y = if(y == -1) {
            null
        } else {
            y
        }
    }

    override fun getJSONObject() : JSONObject {
        var jo = super.getJSONObject();
        if(enemyIndex != null)
            jo.put(ENEMY_INDEX_KEY, enemyIndex!!)
        return jo
    }
}