package com.mygdx.game.model

import com.mygdx.game.config.Constants
import com.mygdx.game.helper.Assets
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.abs

// class containing a single level
open class Level {
    companion object {
        const val X_SIZE : Int = 128;
        const val Y_SIZE : Int = 128;
        const val MAX_ENEMIES : Int = 16;
        const val ENEMY_X_INFLUENCE = 32;
        const val ENEMY_Y_INFLUENCE = 32;

        const val DEFAULT_START_X = -1;
        const val DEFAULT_START_Y = -1;

        const val LEVEL_NAME_KEY = "level_name"
        const val START_LOC_KEY = "start_location"
        const val BLOCK_DATA_KEY = "block_data"
        const val CLIMBABLE_DATA_KEY = "climbable_data"
        const val ITEM_DATA_KEY = "item_data"
        const val OBSTACLE_DATA_KEY = "obstacle_data"
        const val ANIMBLOCK_DATA_KEY = "animblock_data"
        const val DARK_AREAS_DATA_KEY = "dark_areas_data"
        const val ENEMY_DATA_KEY = "enemy_data"
        const val LEVEL_X_KEY = "level_x"
        const val LEVEL_Y_KEY = "level_y"
    }

    var levelName : String = ""

    // player start location
    var startX = DEFAULT_START_X
    var startY = DEFAULT_START_Y

    // position editor is on level (for save)
    var levelX = 0
    var levelY = 0

    // each layer is a multi dimensional list of integers used to identify which block to use
    private lateinit var blockData : MutableList<MutableList<Int>>
    private lateinit var climbableData : MutableList<MutableList<Int>>
    private lateinit var obstacleData : MutableList<MutableList<Int>>
    private lateinit var itemData : MutableList<MutableList<Int>>
    private lateinit var animBlockData : MutableList<MutableList<Int>>

    // this is for storing dark zones, ie. zones needing a torch to navigate
    var darkAreasData = mutableListOf<RectCoords>()

    // enemy data in the form of locations
    lateinit var enemyData : MutableList<EnemyLocation>

    // needed to make sure that each block is separately animated
    // (later I can make each block start at a random point so each animation doesn't look identical to the next)
    private lateinit var animatedBlocksArray : MutableList<MutableList<AnimBlock?>>

    // initialise all above properties
    open fun initialiseProperties() {
        blockData = mutableListOf<MutableList<Int>>()
        climbableData = mutableListOf<MutableList<Int>>()
        obstacleData = mutableListOf<MutableList<Int>>()
        itemData = mutableListOf<MutableList<Int>>()
        animBlockData = mutableListOf<MutableList<Int>>()

        animatedBlocksArray = mutableListOf<MutableList<AnimBlock?>>()

        for(y in 0 until Y_SIZE) {
            var blockRow = mutableListOf<Int>()
            var climbableRow = mutableListOf<Int>()
            var obstacleRow = mutableListOf<Int>()
            var itemRow = mutableListOf<Int>()
            var animLayerRow = mutableListOf<Int>()

            var animBlocksRow = mutableListOf<AnimBlock?>()

            for(x in 0 until X_SIZE) {
                blockRow.add(0)
                climbableRow.add(0)
                obstacleRow.add(0)
                itemRow.add(0)
                animLayerRow.add(0)

                animBlocksRow.add(null)
            }

            blockData.add(blockRow)
            climbableData.add(climbableRow)
            obstacleData.add(obstacleRow)
            itemData.add(itemRow)
            animBlockData.add(animLayerRow)

            animatedBlocksArray.add(animBlocksRow)
        }

        enemyData = mutableListOf<EnemyLocation>()
        for(i in 0 until MAX_ENEMIES) {
            var enemyLocation = EnemyLocation()
            enemyData.add(enemyLocation)
        }

        //var testEnemy = EnemyLocation(0, 10, 10)
        //.add(testEnemy)
    }

    // had to do this way because init block executes after constructors
    constructor() {
        initialiseProperties()
    }

    // accepts all data from a JSON object (for loading data from disk)
    constructor(jo : JSONObject) {
        initialiseProperties()

        // get level name
        if(jo.has(LEVEL_NAME_KEY)) {
            levelName = jo.getString(LEVEL_NAME_KEY)
        }

        // get start location
        if(jo.has(START_LOC_KEY)) {
            var startLocation = Location(jo.getJSONObject(START_LOC_KEY))
            startX = if(startLocation.x == null) {
                -1
            } else {
                startLocation.x!!
            }
            startY = if(startLocation.y == null) {
                -1
            } else {
                startLocation.y!!
            }
        }

        // get block data
        if(jo.has(BLOCK_DATA_KEY)) {
            blockData = mutableListOf<MutableList<Int>>()
            var blockDataJA = jo.getJSONArray(BLOCK_DATA_KEY)
            for(i in 0 until blockDataJA.length()) {
                var row = mutableListOf<Int>()
                var rowJA = blockDataJA.getJSONArray(i)
                for(j in 0 until rowJA.length()) {
                    row.add(rowJA.getInt(j))
                }
                blockData.add(row);
            }
        }

        // get climbable data
        if(jo.has(CLIMBABLE_DATA_KEY)) {
            climbableData = mutableListOf<MutableList<Int>>()
            var climbableDataJA = jo.getJSONArray(CLIMBABLE_DATA_KEY)
            for(i in 0 until climbableDataJA.length()) {
                var row = mutableListOf<Int>()
                var rowJA = climbableDataJA.getJSONArray(i)
                for(j in 0 until rowJA.length()) {
                    row.add(rowJA.getInt(j))
                }
                climbableData.add(row)
            }
        }

        // get item data
        if(jo.has(ITEM_DATA_KEY)) {
            itemData = mutableListOf<MutableList<Int>>()
            var itemDataJA = jo.getJSONArray(ITEM_DATA_KEY)
            for(i in 0 until itemDataJA.length()) {
                var row = mutableListOf<Int>()
                var rowJA = itemDataJA.getJSONArray(i)
                for(j in 0 until rowJA.length()) {
                    row.add(rowJA.getInt(j))
                }
                itemData.add(row);
            }
        }

        // get obstacle data
        if(jo.has(OBSTACLE_DATA_KEY)) {
            obstacleData = mutableListOf<MutableList<Int>>()
            var obstacleDataJA = jo.getJSONArray(OBSTACLE_DATA_KEY)
            for(i in 0 until obstacleDataJA.length()) {
                var row = mutableListOf<Int>()
                var rowJA = obstacleDataJA.getJSONArray(i)
                for(j in 0 until rowJA.length()) {
                    row.add(rowJA.getInt(j))
                }
                obstacleData.add(row)
            }
        }

        // add animation data layer data (layer and AnimBlocks at the same time)
        if(jo.has(ANIMBLOCK_DATA_KEY)) {
            animBlockData = mutableListOf<MutableList<Int>>()
            animatedBlocksArray = mutableListOf<MutableList<AnimBlock?>>()
            var animBlockDataJA = jo.getJSONArray(ANIMBLOCK_DATA_KEY)
            for(i in 0 until animBlockDataJA.length()) {
                var row = mutableListOf<Int>()
                var animatedBlockRow = mutableListOf<AnimBlock?>()
                var rowJA = animBlockDataJA.getJSONArray(i)
                for(j in 0 until rowJA.length()) {
                    row.add(rowJA.getInt(j))
                    if(rowJA.getInt(j) > 0) {
                        animatedBlockRow.add(AnimBlock(Assets.animList.get(rowJA.getInt(j)-1)))
                    } else {
                        animatedBlockRow.add(null)
                    }
                }
                animBlockData.add(row)
                animatedBlocksArray.add(animatedBlockRow)
            }
        }

        // get enemy data
        if(jo.has(ENEMY_DATA_KEY)) {
            enemyData = mutableListOf<EnemyLocation>()
            var enemyDataJA = jo.getJSONArray(ENEMY_DATA_KEY)
            for(i in 0 until enemyDataJA.length()) {
                var enemyLocationJO = enemyDataJA.getJSONObject(i)
                var enemyLocation = EnemyLocation(enemyLocationJO)
                if(enemyLocation.isEnemyActive()) {
                    if(enemyLocation.enemyIndex!! >= Constants.ENEMIES_COUNT) {
                        enemyLocation.enemyIndex = Constants.ENEMIES_COUNT - 1
                    }
                }
                enemyData.add(enemyLocation)
            }
        }

        // get dark area data
        if(jo.has(DARK_AREAS_DATA_KEY)) {
            darkAreasData = mutableListOf<RectCoords>()
            var darkAreasJA = jo.getJSONArray(DARK_AREAS_DATA_KEY)
            for(i in 0 until darkAreasJA.length()) {
                darkAreasData.add(RectCoords(darkAreasJA.getJSONObject(i)))
            }
        }

        // get editor level coordinates
        if(jo.has(LEVEL_X_KEY)) {
            levelX = jo.getInt(LEVEL_X_KEY)
        }
        if(jo.has(LEVEL_Y_KEY)) {
            levelY = jo.getInt(LEVEL_Y_KEY)
        }
    }

    // get start XY
    public fun getStartXY() : Location {
        return Location(startX, startY);
    }

    // set start XY
    public fun setStartXY(x : Int, y : Int) {
        startX = x
        startY = y
    }

    // set start XY with Location
    public fun setStartXY(location : Location) {
        startX = if(location.x == null) {
            -1
        } else {
            location.x!!
        }
        startY = if(location.y == null) {
            -1
        } else {
            location.y!!
        }
    }

    // custom getters and setters for layers using x-y coordinates
    public fun getBlockXY(x : Int, y : Int) : Int {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return abs((x + y) % 4) + 3
        }
        return blockData[y][x]
    }

    public fun setBlockXY(x : Int, y : Int, value : Int) {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return;
        }
        blockData[y][x] = value
    }

    public fun getClimbableXY(x : Int, y : Int) : Int {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return 0;
        }
        return climbableData[y][x]
    }

    public fun setClimbableXY(x : Int, y : Int, value : Int) {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return;
        }
        climbableData[y][x] = value
    }

    public fun getObstacleXY(x : Int, y : Int) : Int {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return 0;
        }
        return obstacleData[y][x]
    }

    public fun setObstacleXY(x : Int, y : Int, value : Int) {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return;
        }
        obstacleData[y][x] = value
    }

    public fun getItemXY(x : Int, y : Int) : Int {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return 0;
        }
        return itemData[y][x]
    }

    public fun setItemXY(x : Int, y : Int, value : Int) {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return;
        }
        itemData[y][x] = value
    }

    public fun getAnimXY(x : Int, y : Int) : Int {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return 0;
        }
        return animBlockData[y][x]
    }

    public fun setAnimXY(x : Int, y : Int, value : Int) {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return;
        }
        animBlockData[y][x] = value
        if(value != 0) {
            animatedBlocksArray[y][x] = AnimBlock(Assets.animList[value-1])
        } else {
            animatedBlocksArray[y][x] = null
        }
    }

    public fun getAnimBlockXY(x : Int, y : Int) : AnimBlock? {
        if(x < 0 || y < 0 || x >= X_SIZE || y >= Y_SIZE) {
            return null;
        }
        return animatedBlocksArray[y][x]
    }

    // for adding and getting enemies
    public fun addEnemy(enemyLocation : EnemyLocation) : Boolean {
        for(i in 0 until MAX_ENEMIES) {
            if(!enemyData[i].isEnemyActive()) {
                if(enemyLocation.isEnemyInBounds()) {
                    enemyData[i] = enemyLocation;
                    return true;
                }
            }
        }
        return false;
    }

    public fun getEnemies() : MutableList<EnemyLocation> {
        return enemyData;
    }

    // gets ememies if they are in area of influence
    public fun getEnemyWithinArea(location : Location) : MutableList<Location> {
        var foundEnemies = mutableListOf<Location>()
        for(i in 0 until MAX_ENEMIES) {
            if(enemyData[i].isEnemyActive()) {
                val diffX : Int = abs(location.x?.minus(enemyData[i].x!!) ?: 0)
                val diffY : Int = abs(location.y?.minus(enemyData[i].y!!) ?: 0)
                if(diffX < ENEMY_X_INFLUENCE / 2) {
                    if(diffY < ENEMY_Y_INFLUENCE / 2) {
                        foundEnemies.add(enemyData[i])
                    }
                }
            }
        }
        return foundEnemies;
    }

    public fun deleteEnemy(x : Int, y : Int) : Boolean {
        for(i in 0 until enemyData.size) {
            if(enemyData[i].isEnemyActive()) {
                if(enemyData[i].x!! == x && enemyData[i].y!! == y) {
                    enemyData[i] = EnemyLocation()
                    return true
                }
            }
        }
        return false
    }

    // set dark area
    public fun setDarkArea(rectCoords: RectCoords) {
        darkAreasData.add(rectCoords)
    }

    // remove dark area
    public fun deleteDarkArea(x : Int, y : Int) : Boolean {
        for(i in darkAreasData.size-1 downTo 0) {
            if(darkAreasData[i].isWithinBounds(x, y)) {
                darkAreasData.removeAt(i)
                return true
            }
        }
        return false
    }

    // gets all data as a JSON Object
    open fun getJSONObject() : JSONObject {
        var jo : JSONObject = JSONObject()

        // put level name in json object
        jo.put(LEVEL_NAME_KEY, levelName)

        // put player start location
        jo.put(START_LOC_KEY, Location(startX, startY).getJSONObject())

        // two-dimensional JSON array for blocks
        var blockDataJA = JSONArray()
        for(row in blockData) {
            var rowJA = JSONArray()
            for(block in row) {
                rowJA.put(block)
            }
            blockDataJA.put(rowJA)
        }
        jo.put(BLOCK_DATA_KEY, blockDataJA)

        // two-dimensional JSON array for climbables
        var climbableDataJA = JSONArray()
        for(row in climbableData) {
            var rowJA = JSONArray()
            for(block in row) {
                rowJA.put(block)
            }
            climbableDataJA.put(rowJA)
        }
        jo.put(CLIMBABLE_DATA_KEY, climbableDataJA)

        // two-dimensional JSON array for items
        var itemDataJA = JSONArray()
        for(row in itemData) {
            var rowJA = JSONArray()
            for(block in row) {
                rowJA.put(block)
            }
            itemDataJA.put(rowJA)
        }
        jo.put(ITEM_DATA_KEY, itemDataJA)

        // two-dimensional JSON array for obstacles
        var obstacleDataJA = JSONArray()
        for(row in obstacleData) {
            var rowJA = JSONArray()
            for(block in row) {
                rowJA.put(block)
            }
            obstacleDataJA.put(rowJA)
        }
        jo.put(OBSTACLE_DATA_KEY, obstacleDataJA)

        // add animation data layer data
        var animBlockDataJA = JSONArray()
        for(row in animBlockData) {
            var rowJA = JSONArray()
            for(block in row) {
                rowJA.put(block)
            }
            animBlockDataJA.put(rowJA)
        }
        jo.put(ANIMBLOCK_DATA_KEY, animBlockDataJA)

        var enemyDataJA = JSONArray()
        for(enemy in enemyData) {
            enemyDataJA.put(enemy.getJSONObject())
        }
        jo.put(ENEMY_DATA_KEY, enemyDataJA)

        var darkAreasJA = JSONArray()
        for(twoCoords in darkAreasData) {
            darkAreasJA.put(twoCoords.getJSONObject())
        }
        jo.put(DARK_AREAS_DATA_KEY, darkAreasJA)

        jo.put(LEVEL_X_KEY, levelX)
        jo.put(LEVEL_Y_KEY, levelY)

        return jo
    }
}