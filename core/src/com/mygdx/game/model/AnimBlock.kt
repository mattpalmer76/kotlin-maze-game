package com.mygdx.game.model

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.mygdx.game.config.Constants
import kotlin.random.Random

// this class encapsulates a sprite and keeps an individual frame count
class AnimBlock(
        // texture atlas where frames are coming from
        var textureAtlas : TextureAtlas,
        // number of frames to use (default 8)
        var frames : Int = 8,
        // for slowing down frames, higher the value the slower the animation
        var slowdownMultiplier : Int = 4,
        // defaults to a random starting position
        var randomAnimStart : Boolean = true
    ) {

    // count frames
    private var frameCount : Int = 0
    private set(value) {
        field = when {
            value >= (frames * slowdownMultiplier) -> {
                0
            }
            value < 0 -> {
                (frames * slowdownMultiplier) - 1
            }
            else -> {
                value
            }
        }
    }

    init {
        // assign a random number starting frame so all anim blocks (eg fire) don't look so similar
        if(randomAnimStart) {
            frameCount = Random.nextInt(0, frames - 1) * slowdownMultiplier
        }
    }

    // draws the block onscreen wherever it's needed and increments frame number
    public fun draw(x : Float, y : Float, batch : Batch) {
        var sprite = Sprite(textureAtlas.findRegion((frameCount/slowdownMultiplier).toString()))
        sprite.setPosition(x, y)
        sprite.setSize(Constants.SCALED_TILE_WIDTH.toFloat(), Constants.SCALED_TILE_HEIGHT.toFloat())
        sprite.draw(batch)
        frameCount++
    }

    // draws onscreen like above but doesn't set size (so any scaling won't work)
    public fun drawNormal(x : Float, y : Float, batch : Batch) {
        var sprite = Sprite(textureAtlas.findRegion((frameCount/slowdownMultiplier).toString()))
        sprite.setPosition(x, y)
        sprite.draw(batch)
        frameCount++
    }
}