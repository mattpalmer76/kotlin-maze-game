package com.mygdx.game.model

import com.mygdx.game.config.Constants
import org.json.JSONObject

// simple class containing an X/Y coordinate that can be retrieved as a JSON
open class Location(var x : Int? = null, var y : Int? = null) {

    companion object {
        val X_KEY = "x"
        var Y_KEY = "y"
    }

    // constructor to accept json object - for loading data from disk
    constructor(jo : JSONObject) : this() {
        x = if(jo.has(X_KEY)) {
            jo.getInt(X_KEY)
        } else {
            null;
        }
        y = if(jo.has(Y_KEY)) {
            jo.getInt(Y_KEY)
        } else {
            null
        }
    }

    public fun getXOnLevel() : Int? {
        if(x != null) {
            return x!! * Constants.SCALED_TILE_WIDTH
        }
        return null;
    }

    public fun getYOnLevel() : Int? {
        if(y != null) {
            return y!! * Constants.SCALED_TILE_HEIGHT
        }
        return null;
    }

    public fun checkCoordsInLevelBounds() : Boolean {
        var xInBounds = false;
        var yInBounds = false;
        if(x != null) {
            if(x!! >= 0 && x!! < Level.X_SIZE) {
                xInBounds = true
            }
        }
        if(y != null) {
            if(y!! >= 0 && y!! < Level.Y_SIZE) {
                yInBounds = true
            }
        }
        return xInBounds && yInBounds
    }

    // class as a JSON object
    public open fun getJSONObject() : JSONObject {
        var jo = JSONObject()

        if(x != null)
            jo.put(X_KEY, x!!)
        else
            jo.put(X_KEY, -1)

        if(y != null)
            jo.put(Y_KEY, y!!)
        else
            jo.put(Y_KEY, -1)

        return jo;
    }
}