package com.mygdx.game

import com.badlogic.gdx.Game
import com.mygdx.game.helper.Assets
import com.mygdx.game.helper.GameFiles
import com.mygdx.game.screens.HomeMenuViewing
import com.mygdx.game.screens.LevelEditViewing
import net.spookygames.gdx.nativefilechooser.NativeFileChooser

// main game class - accepts relevant file chooser based on platform (in this case, just desktop)
class DragonGame(var fileChooser: NativeFileChooser) : Game() {
    override fun create() {
        // prep assets
        Assets.getAssets()

        // prep directories if need be
        GameFiles.checkIfDirs()

        // load list of levels if there is one
        GameFiles.loadLevelSelection()

        // get home menu screen
        setScreen(HomeMenuViewing(this))
    }


    override fun dispose() {
        // dispose of all loaded assets
        Assets.dispose()
    }
}